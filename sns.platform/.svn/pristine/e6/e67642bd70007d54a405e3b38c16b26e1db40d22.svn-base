/**
 *(주)오픈잇 | http://www.openir.co.kr 
 * Copyright (c)2006-2016 openit Inc.
 * All rights reserved
 */
package sns.platform.biz.board;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.inject.Inject;

import org.apache.commons.lang3.math.NumberUtils;

import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import sns.platform.biz.file.FileDAO;
import sns.platform.biz.file.FileService;
import sns.platform.biz.reply.ReplyDAO;
import sns.platform.common.constant.ConstantDefined;
import sns.platform.common.resolver.AttrMap;

@Service
public class BoardService {

	@Inject
	FileDAO fdao;
	@Inject
	BoardDAO dao;
	@Inject
	ReplyDAO rdao;

	
	public BoardService() {
		super();
	}

	public int createBoard(AttrMap map) {
		int boardNum = dao.insert(map.getMap());
		if (map.getFiles() != null && !map.getFiles().isEmpty()) {
			map = this.createBinaryFile(map);
			List<Map<String, Object>> files = (List<Map<String, Object>>) map.get("fileList");
			for (Map<String, Object> file : files) {
				file.put("boardNum", boardNum);
				fdao.insert(file);
			}
			dao.update(map.getMap());
		}
		return 1;
	}
	
	
		
	public Map<String, Object> searchBoard(int boardNum) {
		return dao.selectOne(boardNum);
	}

	public List<Map<String, Object>> searchBoards(AttrMap map) {
		List<Map<String, Object>> list = dao.selectList(map.getMap());
		for (Map<String, Object> board : list) {
			String url = ConstantDefined.HOST_ADDRESS+"/board/"+board.get("boardNum")+"/file/"+board.get("boardFirstImg")+"";
			board.remove("boardFirstImg");
			board.put("firstImageUrl", url);
		}
		return list;
	}

	public int removeBoard(AttrMap map) {

		
		
		/**
		 * 파일 바이너리 지우기
		 */
		
		
		
		fdao.deleteList(map.getMap());
		rdao.deleteList(map.getMap());
		dao.delete(map.getMap());
		return 0;
	}

	
	
	
	public int modifyBoard(AttrMap map) {
		Object temp = map.get("delFiles");
		List<Integer> delFiles = null;
		List<String> delFilePaths = new ArrayList<>();
		List<MultipartFile> files = map.getFiles();
		int boardNum = (int) map.get("boardNum");
		// 업로드 파일이 있다면
		if (files != null && !files.isEmpty()) {
			map = this.createBinaryFile(map);
			List<Map<String, Object>> filelist = (List<Map<String, Object>>) map.get("fileList");
			for (Map<String, Object> file : filelist) {
				file.put("boardNum", boardNum);
				fdao.insert(file);
			}
		}
		
		
		// 지울 파일이 있다면+지울 파일 바이너리 지우기(바이너리추가)
		int fileNum = 0;
		if (temp != null) {
			delFiles = new ArrayList<>();
			if (temp.getClass() == String.class) {
				delFiles.add(NumberUtils.toInt((String) temp, -1));
			} else {
				String[] fileNums = (String[]) temp;
				for (String str : fileNums) {
					fileNum = NumberUtils.toInt(str, -1);
					Map<String, Object> fileVO = fdao.selectOne(fileNum);
					if(fileVO != null){
						delFilePaths.add(ConstantDefined.LOCAL_FILE_PATH+fileVO.get("filePath"));
						delFiles.add(NumberUtils.toInt(str, -1));
						System.out.println("지울번호 "+NumberUtils.toInt(str, -1));
					}
				}
			}
			
			map.put("delFiles", delFiles);
			fdao.deleteFiles(map.getMap());
		}
		dao.update(map.getMap());
		
		File deleteFile = null;
		for (String path : delFilePaths) {
			deleteFile = new File(path);
			deleteFile.delete();
		}
		
		return 1;
	}

	public AttrMap createBinaryFile(AttrMap map) {
		List<Map<String, Object>> fileList = new ArrayList<>();
		List<MultipartFile> files = map.getFiles();
		String filePath = null;
		File binaryFile = null;
		int boardImgCnt = 0;
		int boardAudCnt = 0;
		int boardVidCnt = 0;
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");// 20161110
		String dirPath = ConstantDefined.LOCAL_FILE_PATH + ConstantDefined.BOARD_DIR + format.format(date);// 디렉토리path
		File dir = new File(dirPath);
		dir.mkdirs();
		for (MultipartFile file : files) {
			filePath = ConstantDefined.BOARD_DIR +format.format(date)+"\\"+ UUID.randomUUID().toString() + "_" + file.getOriginalFilename();
			binaryFile = new File(ConstantDefined.LOCAL_FILE_PATH + filePath);
			try {
				file.transferTo(binaryFile);
			} catch (IllegalStateException | IOException e) {
				e.printStackTrace();
				continue;
			}

			if (file.getContentType().contains("audio")) {
				boardAudCnt++;
			} else if (file.getContentType().contains("video")) {
				boardVidCnt++;
			} else if (file.getContentType().contains("image")) {
				boardImgCnt++;
			}

			Map<String, Object> fileVO = new HashMap<>();
			fileVO.put("fileName", file.getOriginalFilename());
			fileVO.put("filePath", filePath);
			fileVO.put("fileType", file.getContentType());
			fileVO.put("fileSize", file.getSize() / 1024.0);
			fileList.add(fileVO);
		}
		map.put("fileList", fileList);
		map.put("boardImgCnt", boardImgCnt);
		map.put("boardAudCnt", boardAudCnt);
		map.put("boardVidCnt", boardVidCnt);
		return map;

	}

}
