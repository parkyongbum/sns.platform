package sns.platform.biz.reply;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.servlet.ModelAndView;

import ch.qos.logback.core.pattern.Converter;
import sns.platform.common.constant.AttrMap;
import sns.platform.common.constant.EntityDTO;

@Controller
public class ReplyController {

	private static final Logger logger = LoggerFactory.getLogger(ReplyController.class);

	@Inject
	private ReplyService service;

	public ReplyController() {
		super();
	}

	@RequestMapping(value = "/board/{boardNum}/reply", method = RequestMethod.POST)
	public ModelAndView createReply(@PathVariable("boardNum") String boardNum, AttrMap map) {
		logger.info("댓글 등록 boardNum " + boardNum + "  " + map.toMapString());
		EntityDTO dto = new EntityDTO();
		int boardNumber = NumberUtils.toInt(boardNum, -1);
		if (boardNumber < 0) {
			return new ModelAndView("jsonView", dto.getDTO());
		}
		map.put("boardNum", boardNumber);
		if (map.checkParam("replyContent")) {
			map.put("userNum", 8);
			dto.setCode(service.createReply(map));// 게시글 3번으로 무조건 입력(댓글 입력 확인위해0
			return new ModelAndView("jsonView", dto.getDTO());
		}
		return new ModelAndView("jsonView", dto.getDTO());
	}

	@RequestMapping(value = "/board/{boardNum}/reply/{replyNum}", method = RequestMethod.POST)
	public ModelAndView modifyReply(AttrMap map, @PathVariable("boardNum") String boardNum,
			@PathVariable("replyNum") String replyNum) {
		logger.info("댓글 수정 boardNum " + boardNum + "replynum " + replyNum + " replyContent " + map.get("replyContent"));
		EntityDTO dto = new EntityDTO();
		int boardNumber = NumberUtils.toInt(boardNum, -1);
		int replyNumber = NumberUtils.toInt(replyNum, -1);
		if (boardNumber < 0 || replyNumber < 0) {
			return new ModelAndView("jsonView", dto.getDTO());
		}
		map.put("boardNum", boardNumber);
		map.put("replyNum", replyNumber);
		if (map.checkParam("replyContent")) {
			dto.setCode(service.modifyReply(map));
			return new ModelAndView("jsonView", dto.getDTO());
		}
		return new ModelAndView("jsonView", dto.getDTO());
	}


	@RequestMapping(value = "/board/{boardNum}/reply/{replyNum}", method = RequestMethod.DELETE)
	public ModelAndView deleteReply(@PathVariable("boardNum") String boardNum,
			@PathVariable("replyNum") String replyNum, AttrMap map) {
		logger.info("댓글 삭제 boardNum " + boardNum + "replynum " + replyNum);
		EntityDTO dto = new EntityDTO();
		int boardNumber = NumberUtils.toInt(boardNum, -1);
		int replyNumber = NumberUtils.toInt(replyNum, -1);
		map.put("boardNum", boardNumber);
		map.put("replyNum", replyNumber);
		dto.setCode(service.removeReply(map));
		return new ModelAndView("jsonView", dto.getDTO());
	}

	@RequestMapping(value = "/board/{boardNum}/replys", method = RequestMethod.GET)
	public ModelAndView searchReplys(@PathVariable("boardNum") String boardNum, AttrMap map) {
		logger.info("댓글 리스트 boardNum " + boardNum + "lastReplyNum " + map.get("lastReplyNum") + " pageNum "
				+ map.get("pageNum"));
		EntityDTO dto = new EntityDTO();
		int boardNumber = NumberUtils.toInt(boardNum, -1);
		List<Map<String, Object>> data = null;
		map.put("boardNum", boardNumber);
		if (map.checkParam("pageNum", "lastReplyNum")) {
			map.put("pageNum", NumberUtils.toInt((String) map.get("pageNum"), -1));
			map.put("lastReplyNum", NumberUtils.toInt((String) map.get("lastReplyNum"), -1));

		} else {
			map.put("pageNum", 10);
			map.put("lastReplyNum", -1);
		}
		data = service.searchReplys(map);
		if (data == null || data.isEmpty()) {
			return new ModelAndView("jsonView", dto.getDTO());
		}
		dto.setCode(0);
		dto.setData(data);
		return new ModelAndView("jsonView", dto.getDTO());
	}

}
