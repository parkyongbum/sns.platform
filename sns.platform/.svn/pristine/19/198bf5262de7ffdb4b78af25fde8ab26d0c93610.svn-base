package sns.platform.biz.file;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
/**
 *(주)오픈잇 | http://www.openir.co.kr 
 * Copyright (c)2006-2016 openit Inc.
 * All rights reserved
 */
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.servlet.ModelAndView;

import sns.platform.common.constant.EntityDTO;
import sns.platform.common.resolver.AttrMap;

@Controller
public class FileController {

	private static final Logger logger = LoggerFactory.getLogger(FileController.class);

	@Inject
	private FileService service;

	public FileController() {
		super();
	}

	@RequestMapping(value = "/board/{boardNum}/files", method = RequestMethod.GET)
	public ModelAndView searchFiles(@PathVariable("boardNum") String boardNum) {
		logger.info("파일리스트" + boardNum + "");
		EntityDTO dto = new EntityDTO();
		int boardNumber = NumberUtils.toInt(boardNum, -1);
		List<Map<String, Object>> data = service.searchFiles(boardNumber);
		if (data == null || data.isEmpty()) {
			return new ModelAndView("jsonView", dto.getDTO());
		}
		dto.setData(data);
		dto.setCode(0);
		return new ModelAndView("jsonView", dto.getDTO());
	}

	@RequestMapping(value = "/board/{boardNum}/file/{fileNum}", method = RequestMethod.GET)
	public ModelAndView fileDown(@PathVariable("boardNum") String boardNum, @PathVariable("fileNum") String fileNum,
			AttrMap map) {
		int boardNumber = NumberUtils.toInt(boardNum, -1);
		int fileNumber = NumberUtils.toInt(fileNum, -1);
		if (boardNumber < 0 || fileNumber < 0) {
			return null;
		}
		map.put("boardNum", boardNumber);
		map.put("fileNum", fileNumber);
		Map<String, Object> file = service.searchFile(map);
		if (file == null) {
			return null;
		}
		logger.info((String)file.get("filePath"));
		map.putAll(file);
		return new ModelAndView("downloadView", map.getMap());
	}
	
	@RequestMapping(value = "/user/{userNum}/image", method = RequestMethod.GET)
	public ModelAndView profileDown(@PathVariable("userNum") String userNum) {
		int userNumber = NumberUtils.toInt(userNum, -1);
		Map<String, Object> map = service.searchProfile(userNumber);
		if(map == null){
			return null;
		}
		return new ModelAndView("downloadView", map);
	}

}
