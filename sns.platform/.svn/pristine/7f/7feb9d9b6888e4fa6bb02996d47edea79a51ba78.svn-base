/**
 *(주)오픈잇 | http://www.openir.co.kr 
 * Copyright (c)2006-2016 openit Inc.
 * All rights reserved
 */
package sns.platform.biz.user;

import java.io.File;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import sns.platform.common.constant.ConstantDefined;
import sns.platform.common.resolver.AttrMap;

@Service
public class UserService {

	@Inject
	private UserDAO dao;
	

	private static final Logger LOG = LoggerFactory.getLogger(UserService.class);

	public UserService() {
		super();
	}

	public int createUser(AttrMap map) {
		LOG.info("파일의 유무 " + (map.getUserFile() != null));
		if (map.getUserFile() != null) {
			MultipartFile userFile = map.getUserFile();
			String userId = (String) map.get("userId");
			int idx = userFile.getOriginalFilename().lastIndexOf(".");
			String extension = userFile.getOriginalFilename().substring(idx);
			File file = new File(ConstantDefined.FILE_PATH + "profile\\" + userId + extension);
			try {
				FileCopyUtils.copy(userFile.getInputStream(), new FileOutputStream(file));
			} catch (IOException e) {
				return 0;
			}
			String URI = ConstantDefined.PROFILE_PATH + userId + extension;
			map.put("userFile", URI);
		}
		dao.insert(map.getMap());
		return 1;
	}

	public Map<String, Object> search(int userNum) {
		return dao.selectOne(userNum);
	}

	public UserVO modifyUser(AttrMap map) {
		if (map.getUserFile() != null) {
			MultipartFile userFile = map.getUserFile();
			String userId = (String) map.get("userId");
			int idx = userFile.getOriginalFilename().lastIndexOf(".");
			String extension = userFile.getOriginalFilename().substring(idx);
			File file = new File(ConstantDefined.FILE_PATH + "profile\\" + userId + extension);
			try {
				FileCopyUtils.copy(userFile.getInputStream(), new FileOutputStream(file));
			} catch (IOException e) {
				return null;
			}
			String URI = ConstantDefined.PROFILE_PATH + userId + extension;
			map.put("userFile", URI);
		}
		int cnt = dao.update(map.getMap());
		if (cnt > 0) {// 성공
			UserVO vo = dao.login(map.getMap());
			return vo;
		}
		return null;
	}

	public int removeUser(int userNum) {
		if (0 < dao.delete(userNum)) {
			return 1;
		}
		return 0;
	}

	public UserVO login(String userId, String userPass) {
		Map<String, Object> map = new HashMap<>();
		map.put("userId", userId);
		map.put("userPass", userPass);
		return dao.login(map);
	}

	public int checkId(String userId) {
		Map<String, Object> map = new HashMap<>();
		map.put("userId", userId);
		int cnt = dao.checkId(map);
		if (cnt > 0) {
			return 0;
		}
		return 1;
	}

	public int checkNick(String userNick) {
		Map<String, Object> map = new HashMap<>();
		map.put("userNick", userNick);
		int cnt = dao.checkName(map);
		if (cnt > 0) {
			return 0;
		}
		return 1;
	}

	public int checkPass(AttrMap map) {
		int cnt = dao.checkPassword(map.getMap());
		if (cnt > 0) {
			return 1;
		}
		return 0;
	}

	public String createProfileUpload(AttrMap map) {
		if (!map.getUserFile().getContentType().contains("image")) {
			return null;
		}
		int idx = map.getUserFile().getOriginalFilename().lastIndexOf(".");
		System.out.println("idx  " + idx);
		String extension = map.getUserFile().getOriginalFilename().substring(idx);
		String filename = map.get("userNum") + extension;
		String savepath = ConstantDefined.FILE_PATH + "profile\\" + filename;

		System.out.println("저장 경로 : " + savepath);
		try {
			FileCopyUtils.copy(map.getUserFile().getBytes(), new FileOutputStream(new File(savepath)));
		} catch (Exception e) {
			System.out.println("저장 실패");
			return null;
		}
		System.out.println("저장 성공");
		return ConstantDefined.PROFILE_PATH + filename;
	}

}
