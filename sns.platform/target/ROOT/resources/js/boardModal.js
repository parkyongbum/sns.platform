var userNum = $('#headerNav .container .collapse .nav .dropdown img').attr('name');
var board = {};
var pageVO = { lastpage : true , page : false}
var replyContens = {}

/**
 * 게시글 하나 클릭시 게시글 상세 모달창 활성화
 */
$(document).on('click','.btn-board-modal-open',function(event){
	initBoard();
	var boardNum = $(this).attr('title')
	$('.modal-board').attr('title',boardNum);
	$('.modal-board').modal('show');
	boardRepage(boardNum);
	replyappend(boardNum);
	pageVO.page=true;
	
});

/**
 * 게시글 수정버튼 클릭시
 */
$(document).on('click','.modal-btn-board-modify',function(event){
	var $modbtn =$('#boardModmodal .modal-dialog .modal-content .modal-footer .btn-board-mod-com');
	$modbtn.addClass('modal-btn-board-modify-com');
	$modbtn.siblings('.modal-close').addClass('modal-btn-board-modify-can');
	$('.modal-board').modal('hide');
	
	
	/**
	 * 수정넣기
	 */
	pageVO.page=false;
});

/**
 * 게시글 수정 취소버튼클릭시
 */
$(document).on('click','.modal-btn-board-modify-can',function(event){
	var $modbtn =$('#boardModmodal .modal-dialog .modal-content .modal-footer .btn-board-mod-com');
	$modbtn.removeClass('modal-btn-board-modify-com');
	$modbtn.siblings('.modal-close').removeClass('modal-btn-board-modify-can');
	$('#boardModmodal').modal('hide');
	$('.modal-board').modal('show');
	pageVO.page=true;
});

/**
 * 게시글 상세보기 닫기 클릭시
 */
$(document).on('click','.btn-board-modal-close',function(event){
	$('.modal-board').modal('hide');
	var boardNum = $('.modal-board').attr('title')
	updateBoardHtml(boardNum)
	pageVO.page=false;
});

/**
 * 게시글 백드랍 클릭시
 */
$(document).on('click','.modal-board .modal-backdrop',function(event){
	var boardNum = $('.modal-board').attr('title')
	updateBoardHtml(boardNum)
})

/**
 * 댓글 입력시
 */
$(document).on('click','.modal-board .modal-body .modal-body-reply .modal-body-reply-header .modal-body-reply-one-reg .modal-body-reply-register',function(event){
	var $textarea = $(this).siblings('.modal-body-reply-one-textarea');
	var replyContent = $textarea.val();
	var boardNum = $('.modal-board').attr('title');
	if(replyContent == '' || replyContent.trim() == '' ) {
		alert('내용을 입력해주세요');
		return;
	}
	$.ajax({
		url : '/board/'+boardNum+'/reply',
		type : 'POST',
		data : { replyContent : replyContent},
		success : function(data) {
				if(data.code>0){
					alert('등록되었습니다');
					$textarea.val('')
					$('.modal-board .modal-dialog .modal-content .modal-body .modal-body-reply-list').html('');
					replyappend(boardNum);
					$.ajax({
						url : '/board/'+boardNum,
						type : 'GET',
						success : function(data) {
								if(data.code>0){
									
									var board = data.data;
									var replycount = board.boardReplyCnt;
									var filecount = board.boardVidCnt+board.boardImgCnt+board.boardAudCnt;
									var counthtml = '<img class="modal-body-img-board-header-count" src="resources/images/replyImg.png" /> '+replycount+
						 							'<img class="modal-body-img-board-header-count" src="resources/images/file.png" />' +filecount+'</div>';
									$('.modal-board .count').html(counthtml)
									
									var starthtml = '<img class="modal-body-img-board-header-count" src= "/resources/images/replyImg.png">'+
													replycount;
									$('.modal-board .modal-body-reply-start').html(starthtml)
								}
							},
						error : function(data) {
							alert('죄송합니다 잠시후에 접속해 주세요');
						}
					});
					
					
				} else {
					alert('죄송합니다 잠시후에 접속해 주세요');
				}
			},
		error : function(data) {
			alert('죄송합니다 잠시후에 접속해 주세요');
		}
	});
});

/**
 * 댓글 수정버튼 클릭시
 */
$(document).on('click','.modal-board .modal-body .modal-body-reply .modal-body-reply-list .modal-body-reply-one .modal-body-reply-one-btn-modyfy',function(event){
	var $textarea = $(this).parent().parent().parent().find('.modal-body-reply-one-body .modal-body-reply-one-body-text');
	var pastContent = $textarea.html();
	var replyNum = $(this).attr('name');
	var modhtml = '<button class="modal-body-reply-one-btn-modyfy-com btn btn-warning btn-xs" name = "'+replyNum+'">완료</button>'+
				  '<button class="modal-body-reply-one-btn-modyfy-can btn btn-danger btn-xs" name = "'+replyNum+'">취소</button>';
	var key = 'reply'+replyNum;
	replyContens[key] = pastContent;
	$textarea.removeClass('modal-body-reply-one-body-text').addClass('modal-body-reply-one-body-text-modyfy');
	$(this).parent().html(modhtml);
});

/**
 * 댓글수정 취소시
 */
$(document).on('click','.modal-board .modal-body .modal-body-reply .modal-body-reply-list .modal-body-reply-one .modal-body-reply-one-btn-modyfy-can',function(event){
	var replyNum = $(this).attr('name');
	var key = 'reply'+replyNum;
	var pastContent = replyContens[key];
	var modhtml = '<button class="modal-body-reply-one-btn-modyfy btn btn-warning btn-xs" name = "'+replyNum+'">U</button>'+
				  '<button class="modal-body-reply-one-btn-delete btn btn-danger btn-xs" name = "'+replyNum+'">X</button>';
	var $textarea = $(this).parent().parent().parent().find('.modal-body-reply-one-body .modal-body-reply-one-body-text-modyfy');
	$textarea.removeClass('modal-body-reply-one-body-text-modyfy').addClass('modal-body-reply-one-body-text');
	$textarea.val(pastContent);
	$(this).parent().html(modhtml);
});

/**
 * 댓글 수정 완료시
 */
$(document).on('click','.modal-board .modal-body .modal-body-reply .modal-body-reply-list .modal-body-reply-one .modal-body-reply-one-btn-modyfy-com',function(event){
	var boardNum = $('.modal-board').attr('title');
	var replyNum = $(this).attr('name');
	var $textarea = $(this).parent().parent().parent().find('.modal-body-reply-one-body .modal-body-reply-one-body-text-modyfy');
	var replyContent = $textarea.val();
	var $this = $(this);
	
	if(replyContent == '' || replyContent.trim() == '' ) {
		alert('내용을 입력해주세요');
		return;
	}
	
	
	
	$.ajax({
		url : '/board/'+boardNum+'/reply/'+replyNum,
		type : 'POST',
		data : { replyContent : replyContent},
		success : function(data) {
				if(data.code>0){
					var modhtml = '<button class="modal-body-reply-one-btn-modyfy btn btn-warning btn-xs" name = "'+replyNum+'">U</button>'+
					  			  '<button class="modal-body-reply-one-btn-delete btn btn-danger btn-xs" name = "'+replyNum+'">X</button>';
					$textarea.val(replyContent);
					$textarea.removeClass('modal-body-reply-one-body-text-modyfy').addClass('modal-body-reply-one-body-text');
					$this.parent().html(modhtml);
					alert('수정되었습니다');
				} else {
					alert('죄송합니다 잠시후에 접속해 주세요');
				}
			},
		error : function(data) {
			alert('죄송합니다 잠시후에 접속해 주세요');
		}
	});
	
});

/**
 * 댓글 삭제 클릭시
 */
$(document).on('click','.modal-board .modal-body .modal-body-reply .modal-body-reply-list .modal-body-reply-one .modal-body-reply-one-btn-delete',function(event){
	var replyNum = $(this).attr('name');
	var boardNum = $('.modal-board').attr('title');
	var $this = $(this);
	$.ajax({
		url : '/board/'+boardNum+'/reply/'+replyNum,
		type : 'DELETE',
		success : function(data) {
				if(data.code>0){
					alert('삭제되었습니다');
					$this.parent().parent().parent().fadeOut(500,function(){
					$this.remove();
					$.ajax({
							url : '/board/'+boardNum,
							type : 'GET',
							success : function(data) {
									if(data.code>0){
										var board = data.data;
										var replycount = board.boardReplyCnt;
										var filecount = board.boardVidCnt+board.boardImgCnt+board.boardAudCnt;
										var counthtml = '<img class="modal-body-img-board-header-count" src="resources/images/replyImg.png" /> '+replycount+
							 							'<img class="modal-body-img-board-header-count" src="resources/images/file.png" />' +filecount+'</div>';
										$('.modal-board .count').html(counthtml)
										
										var starthtml = '<img class="modal-body-img-board-header-count" src= "/resources/images/replyImg.png">'+
														replycount;
										$('.modal-board .modal-body-reply-start').html(starthtml)
									}
								},
							error : function(data) {
								alert('죄송합니다 잠시후에 접속해 주세요');
							}
						});
						
						
					})
				} else {
					alert('죄송합니다 잠시후에 접속해 주세요');
				}
			},
		error : function(data) {
			alert('죄송합니다 잠시후에 접속해 주세요');
		}
	});
	
});



/**
 * 게시글 상세보기 무한스크롤
 */
$('.modal-board').scroll(function () {
	var boardNum = $(this).attr('title')
	if ($('.modal-board').scrollTop() > ($('.modal-board .modal-backdrop').height() - $(window).height())-1) {
		if(pageVO.lastpage) {
			replyappend(boardNum)
		}
	}
	
})



/**
 * 게시글 상세보기 모달창 생성
 * 
 * @param boardNum 게시글번호
 */
function boardRepage (boardNum) {
	$.ajax({
		url : '/board/'+boardNum,
		type : 'GET',
		success : function(data) {
			if(data.code>0){
				board = data.data;//게시판의 정보 및파일의 정보를 모두 포함
				$.ajax({
					url : '/board/'+boardNum+'/files',
					type : 'GET',
					success : function(data) {
												if(data.code>0){
												board.files = data.data;
											    } else {
											    	board.files = null;
											    }
												boardModalMakeHtml(board)
											  },
					error : function(data) {
											alert('죄송합니다 파일을 가져오는중 서버오류입니다 잠시후에 접속해주세요');
										   }
				});
				}
			},
		error : function(data) {
			alert('죄송합니다 잠시후에 접속해 주세요');
		}
	});
	
} 

/**
 * 게시글 파일 보기
 */
$(document).on('click','.btn-file-show',function(event){
	var idx = $(this).attr('name');
	var file = board.files[idx];
	
	var headerhtml = file.fileName+'<button class="close file-close">x</button>'
	$('.div-file-detail .div-file-header').html(headerhtml);
	var bodyhtml = '';
	if	( file.fileType.match('video') ) {
		 bodyhtml = '<video controls="controls" autoplay="autoplay" >'+
		 			'<source  src="'+file.fileUrl+'" type="'+file.fileType+'" />'+
		 			'</video>';
	} else if ( file.fileType.match('audio') ) {
		 bodyhtml = '<audio  controls="controls" autoplay="autoplay" loop="loop" preload="auto">'+
		 			'<source src="'+file.fileUrl+'" type="'+file.fileType+'" />'+
		 			'</audio>'
	} else {
		  bodyhtml = '<img src = "'+file.fileUrl+'" />';
	}
	$('.div-file-detail .div-file-body').html(bodyhtml);
	$('.div-file-detail .div-file-footer a').attr('href',file.fileUrl+'?view=down');
	$('.div-file-detail').css('visibility','visible')
	
})

/**
 * 파일 닫기
 */
$(document).on('click','.file-close',function(event){
		stopfile();
		$('.div-file-detail').css('visibility','hidden')
		console.log($('audio'))
})

/**
 * 파일 멈추기
 *
 */
function stopfile () {
	if($('audio').length > 0){
		$('audio')[0].pause();
	} else if($('video').length > 0){
		$('video')[0].pause();
	}
}
/**
 * 파일 시작하기
 * @returns
 */
function playfile () {
	if($('audio').length > 0){
		$('audio')[0].play();
	} else if($('video').length > 0){
		$('video')[0].play();
	}
}

/**
 * 게시글 모달창 동적 생성
 * @param board 보고싶은 게시글번호
 * @returns
 */
function boardModalMakeHtml(board){
	var userNum = $('#headerNav .container .collapse .nav .dropdown img').attr('name');
	var $boardModal = $('.modal-board');
	$boardModal.attr('title',board.boardNum);
	var $boardModalbody = $boardModal.find('.modal-dialog .modal-content .modal-body');
	
	$boardModal.find('.modal-dialog .modal-content .modal-header div p').html(board.boardNum+'번 게시물 입니다');
	
	
	var modifyhtml = ''
	if (userNum == board.userNum) {
		modifyhtml = '<button class="btn btn-warning btn-xs btn-board-modify modal-btn-board-modify" name ="'+board.boardNum+'" >U</button>'+
						 '<button class="btn btn-danger btn-xs btn-board-del" name ="'+board.boardNum+'">X</button>'
	}
	modifyhtml += ' <button type="button" class="close modal-close btn-board-modal-close">×</button>'
	$boardModal.find('.modal-dialog .modal-content .modal-header .modal-header-modify').html(modifyhtml);
	
	var $modalBody = $boardModal.find('.modal-dialog .modal-content .modal-body');
	var $boardDiv = $modalBody.find('.modal-body-board');
	var fileCnt = board.boardVidCnt+board.boardAudCnt+board.boardImgCnt;
	
	
	$boardDiv.find('.modal-body-img-board-header').attr('src','/user/'+board.userNum+'/image')
	
	var userinfo = ''+board.userNick+'('+board.userId+')'+board.boardUdtDate.substr(2,2)+'/'+board.boardUdtDate.substr(4,2)+'/'+
					board.boardUdtDate.substr(6,2)+' '+board.boardUdtDate.substr(8,2)+'시';
	$boardDiv.find('.modal-body-board-header div').first().append(userinfo)+'';
	
	var counthtml = '<img class="modal-body-img-board-header-count" src= "/resources/images/replyImg.png">'+board.boardReplyCnt+
					'<img class="modal-body-img-board-header-count" src= "/resources/images/file.png">'+fileCnt
	
	$boardDiv.find('.modal-body-board-header .count').html(counthtml)
	
	$boardDiv.find('.modal-body-board-body .modal-body-textarea').html(board.boardContent);
	
	var files = board.files;
	if (files) {
		var fileHtml = '<div class="col-md-12 modal-body-board-footer col-md-12">';
		for (var i = 0 ,len = files.length; i<len; i++) {
			var type = files[i].fileType;
			var src = '';
			  if( type.match('video') ) {
				  src = '/resources/images/video.png'
			  } else if ( type.match('audio') ) {
				  src = '/resources/images/audio.png'
			  } else {
				  src = files[i].fileUrl;
			  }
			fileHtml += '<img class="modal-body-img-board-footer-count btn-file-show" title = "'+files[i].fileName+'"'+
						'src= "'+src+'" name="'+i+'" />'
		}
		fileHtml += '</div>';
		$boardDiv.append(fileHtml);
	}
	
	$boardDiv.parent().find('.modal-body-reply-start').append(board.boardReplyCnt)
}


/**
 * 댓글 html 추가
 * 
 * @param boardNum 게시글 번호
 * @returns
 */
function replyappend (boardNum) {
	var lastReplyNum = $('.modal-body-reply-one').last().attr('title');
	if( !lastReplyNum ) {
		lastReplyNum = -1;
	}
	pageVO.lastpage = false ;
	
	$.ajax({
		url : '/board/'+boardNum+'/replys?pageNum=5&lastReplyNum='+lastReplyNum,
		type : 'GET',
		success : function(data) {
									if(data.code>0){
									var replys = data.data;
									var html = ''
									for (var i = 0, len=replys.length; i<len; i++ ){
										html += replyMakeHtml(replys[i])
									}
									$('.modal-board .modal-dialog .modal-content .modal-body .modal-body-reply-list').append(html);
									var contentheight = $('.modal-board .modal-content').height();
									$('.modal-board .modal-backdrop').height(contentheight+100);
									pageVO.lastpage = true;
									}
								  },
		error : function(data) {
								alert('죄송합니다 파일을 가져오는중 서버오류입니다 잠시후에 접속해주세요');
							   }
	});
	
	
}


/**
 * 댓글 html 만들기
 * @param reply 댓글 정보
 * @returns
 */
function replyMakeHtml (reply) {
	var replyhtml = '<div class="col-md-12 modal-body-reply-one" title="'+reply.replyNum+'">' +
					'<div class="col-md-12 modal-body-reply-one-header">'+
					'<div class="col-md-9">'+
					'<img class="modal-body-reply-one-header-profile" src= "/user/'+reply.userNum+'/image" />'+
					reply.userNick+'('+reply.userId+')'+reply.replyRegDate.substr(2,2)+'/'+reply.replyRegDate.substr(4,2)+'/'+
					reply.replyRegDate.substr(6,2)+' '+reply.replyRegDate.substr(8,2)+
					'</div>';
					
					if (reply.userNum == userNum ){
						replyhtml += '<div class="col-md-3 modal-body-reply-one-modyfy" >'+
									 '<button class="modal-body-reply-one-btn-modyfy btn btn-warning btn-xs" name="'+reply.replyNum+'">U</button>'+
									 '<button class="modal-body-reply-one-btn-delete btn btn-danger btn-xs" name="'+reply.replyNum+'">X</button>'+
									 '</div>'
					}		
					replyhtml += '</div>'+
								 '<div class="col-md-12 modal-body-reply-one-body">'+
								 '<textarea class="modal-body-reply-one-body-text">'+reply.replyContent+'</textarea>'+
								 '</div>'+
								 '</div>'
		
	return 	replyhtml;
}

/**
 * 게시글 모달창 초기화
 * 
 * @returns
 */
function initBoard() {
	
	var html = '<div class="modal-header col-md-12">'+
			   '<div class="col-md-6">'+
			   '<p>죄송합니다</p>'+
			   '</div>'+
			   '<div class="col-md-6 modal-header-modify" >'+
			   '</div>'+
			   '</div>'+
			   '<div class="modal-body append col-md-12">'+
			   '<div class="col-md-12 modal-body-board">'+
			   '<div class="col-md-12 modal-body-board-header">'+
			   '<div class = "col-md-8">'+
			   '<img class="modal-body-img-board-header profile" src= "/resources/images/eror.jpg">'+
			   '</div>'+
			   '<div class = "col-md-4 count">'+
			   '</div>'+
			   '</div>'+
			   '<div class="col-md-12 modal-body-board-body">'+
			   '<textarea class="col-md-12 modal-body-textarea" >삭제된게시글입니다</textarea>'+
			   '</div>'+
			   '</div>'+
			   '<div class="modal-body-reply col-md-12">'+
			   '<div class="modal-body-reply-start col-md-12">'+
			   '<img class="modal-body-img-board-header-count" src= "/resources/images/replyImg.png">'+
			   '</div>'+
			   '<div class="col-md-12 modal-body-reply-header">'+
			   '입력창'+
			   '<div class="col-md-12 modal-body-reply-one modal-body-reply-one-reg">'+
			   '<textarea class="modal-body-reply-one-textarea"></textarea>'+
			   '<button class="btn btn-success modal-body-reply-register">입력</button>'+
			   '</div>'+
			   '</div>'+
			   '<div class="col-md-12 modal-body-reply-start">'+
			   '<img class="modal-body-img-board-header-count" src= "/resources/images/replyImg.png">'+
			   '</div>'+
			   '<div class="col-md-12 modal-body-reply-footer">'+
			   '<div class="col-md-12 modal-body-reply-list">'+
			   '</div>'+
			   '</div>'+
			   '</div>'+
			   '<div class="modal-footer col-md-12">'+
			   '<button type="button" class="btn btn-default modal-close btn-board-modal-close" >닫기</button>'+
			   '</div>'+
			   '</div>';
	
	        $('.modal-board .modal-dialog .modal-content').addClass('col-md-12')
			$('.modal-board .modal-dialog .modal-content').html(html);
}

