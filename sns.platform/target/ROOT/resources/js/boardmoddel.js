/**
 * 게시글 수정 모달시 사용 객체
 */
var modfyBoard = {boardNum : 0, boardContent : null, files  : new Array(), delFiles : new Array(), currentFiles : null  }

/**
 * 수정버튼 클릭시
 */
$(document).on('click','.btn-board-modify',function(e) {
	e.stopPropagation();
	modfyBoard = {boardNum : 0, boardContent : null, files  : new Array(), delFiles : new Array(), currentFiles : null  }
	var boardNum = $(this).attr('value');
	if (!boardNum) {
		boardNum = $('.modal-board').attr('title')
	}
	
	modfyBoard.boardNum = boardNum;
	$.ajax({
		url : '/board/'+boardNum,
		type : 'GET',
		success : function(data) {
			if(data.code>0){
				var boardDTO = data.data;
				modfyBoard.boardContent = boardDTO.boardContent
				$.ajax({
					url : '/board/'+boardNum+'/files',
					type : 'GET',
					success : function(data) {
						if(data.code>0){
							var files = data.data;
							modfyBoard.currentFiles = files;
						} 
						loadModal(modfyBoard);
					},
						error : function(data) {
							alert('죄송합니다 잠시후에 접속해 주세요');
						}
					});
			} else {
				alert('죄송합니다 잠시후에 접속해 주세요');
			}
		},
		error : function(data) {
			alert('죄송합니다 잠시후에 접속해 주세요');
		}
	});
	
});

/**
 * 상세보기 게시글 삭제
 */
$(document).on('click','.btn-board-delete',function(e) {
	e.stopPropagation();
	var boardNum = $(this).attr('value');
	$.ajax({
		url : '/board/'+boardNum,
		type : 'DELETE',
		success : function(data) {
			if (data.code > 0 ) {
				alert('삭제 되었습니다')
				location.href = '/boardView';
			} else {
				alert('죄송합니다 잠시후 다시 접속해주세요')
			}
		},
		error : function(data) {
			alert('죄송합니다 잠시후 다시 접속해주세요')
		}
	})
	
});

/**
 * 게시글 수정시 파일 수정(삭제)
 */
$(document).on('click','#boardModmodal .modal-dialog .modal-content .modal-body .file-cancel',function(e) {
	var fileNum = $(this).attr('value');
	var fileidx = $(this).attr('name');
	if (fileNum) {
		var src = $(this).siblings('img').attr('src');
		$(this).siblings('img').attr('src','/resources/images/eror.jpg')
		$(this).siblings('img').attr('name',fileNum)
		$(this).siblings('img').attr('alt',src)
		$(this).siblings('img').attr('title','파일수정취소')
		$(this).siblings('img').addClass('btn-file-rewind')
		$(this).fadeOut(100)
	} else if (fileidx) {
		modfyBoard.files[fileNum] = null;
		$(this).parent().fadeOut(1000,function(){$(this).remove()});
		
	}
	
});

/**
 * 파일 삭제 취소
 */
$(document).on('click','#boardModmodal .modal-dialog .modal-content .modal-body .btn-file-rewind',function(e) {
	var fileNum = $(this).attr('name');
	var src = $(this).attr('alt');
	$(this).removeClass('btn-file-rewind');
	$(this).attr('src',src);
	$(this).attr('title','')
	$(this).parent().prepend('<button class="btn btn-xs btn-danger file-cancel" value = "'+fileNum+'">X</button>');
});

/**
 * 파일 드랍존 막기
 */
$(document).on('dragenter dragover','#boardModmodal .modal-dialog .modal-content .modal-body .dropzone',function(event){
	event.preventDefault();
});

/**
 * 파일 드랍 이벤트
 */
$(document).on('drop','#boardModmodal .modal-dialog .modal-content .modal-body .dropzone',function(event){
	event.preventDefault();
	var files = event.originalEvent.dataTransfer.files;
	var html = '';
	for (var i = 0, len = files.length; i < len; i++ ) {
		var el = fileHtml(files[i],modfyBoard);
		if (el) {
			html += el; 
		}
	}
	$(this).siblings('.div-files').append(html);
	
});

/**
 * 게시글 수정 완료
 */
$(document).on('click','#boardModmodal .modal-dialog .modal-content .modal-footer .btn-board-mod-com',function(event){
	var boardContent = $(this).parent().parent().find('.modal-body textarea').val();
	var boardNum = modfyBoard.boardNum;
	
	if(!boardContent) {
		alert('글을 입력해주세요')
		return;
	}
	
	var formData = new FormData();
	var appendfiles = modfyBoard.files;
	formData.append('boardContent',boardContent);
	
	if(appendfiles.length > 0 ) {
		for (var i = 0, len = appendfiles.length; i < len; i++ ) {
			if(appendfiles[i]) {
				formData.append('files',appendfiles[i]);
			}
		}
	}
	
	var $imgs = $(this).parent().parent().find('.modal-body .div-files .btn-file-rewind')
	
	if($imgs.length > 0 ) {
		for (var i = 0, len = $imgs.length; i < len; i++ ) {
			var fileNum = $($imgs[i]).attr('name');
			if(fileNum) {
			formData.append('delFiles',fileNum);
			console.log(fileNum);
			}
		}	
	}
	
	$.ajax({
		url : '/board/'+boardNum,
		data : formData,
		processData : false,
		contentType : false,
		type : 'POST',
		success : function(data) {
			if (data.code > 0 ) {
				initBoard();
				$('.modal-board').attr('title',boardNum);
				$('.modal-board').modal('show');
				boardRepage(boardNum);
				replyappend(boardNum);
				$('#boardModmodal').modal('hide')
				$('.modal-btn-board-modify-com').removeClass('modal-btn-board-modify-com');
				$('.modal-btn-board-modify-can').removeClass('modal-btn-board-modify-can');
				
				pageVO.page=false;
				updateBoardHtml(boardNum)
				
			} else {
				alert('죄송합니다 잠시후 다시 접속해주세요')
			}
		},
		error : function(data) {
			alert('죄송합니다 잠시후 다시 접속해주세요')
		}
	})
	
	
});

/**
 * 수정된 게시글 html 만들어서 널기 
 */
function updateBoardHtml(boardNum){
	var lastboardNum = Number(boardNum)+1
	$.ajax({
		url : '/boards?pageNum=1&lastBoardNum='+lastboardNum,
		type : 'GET',
		success : function(data) {
			if(data.code>0){
				var board = data.data[0];
				var html = boardHtmlmaker2(board)
				$('.div-board[title="'+board.boardNum+'"').html(html);
			}
		},
		error : function(data) {
			alert('죄송합니다 잠시후에 접속해 주세요');
		}
	});
}







/**
 * 
 * @param board 수정 게시글의 정보
 * @returns 모달창 로드
 */
function loadModal (board) {
	$('#boardModmodal .modal-dialog .modal-content .modal-body textarea').val(board.boardContent);
	var filehtml = ''
		if(board.currentFiles) {
			for (var i = 0 , len = board.currentFiles.length; i < len; i++ ) {
				var file = board.currentFiles[i]
				var src = '';
				if (file.fileType.match('image')) {
					src = file.fileUrl;
				} else if (file.fileType.match('video')) {
					src = '/resources/images/video.png';
				} else if (file.fileType.match('audio')) {
					src = '/resources/images/audio.png';
				} 
				filehtml += '<div class="col-md-2 file-div" >'
				+'<button class="btn btn-xs btn-danger file-cancel" value = "'+file.fileNum+'">X</button>'
				+'<img src="'+src+'" /><br>'
				+file.fileName+
				'</div>'
			}
		}
	
	
	
	$('#boardModmodal .modal-dialog .modal-content .modal-body .div-files').html(filehtml);
	$('#boardModmodal').modal();
	
}

/**
 * 
 * @param file 파일의 정보
 * @param boardVO 전역변수
 * @returns 파일의 HTML
 */
function fileHtml (file,boardVO){
	var files = boardVO.files;
	var fileName = file.name;
	var src = '';
	file.idx = files.length;
	if (file.type.match('image')) {
		files.push(file);
		blobURL = window.URL.createObjectURL(file);
		src = blobURL;
	} else if (file.type.match('video')) {
		files.push(file);
		src = '/resources/images/video.png';
	} else if (file.type.match('audio')) {
		files.push(file);
		src = '/resources/images/audio.png';
	} else {
		return null;
	}
	var html = 
		'<div class="col-md-2 file-div" >'
		+'<button class="btn btn-xs btn-danger file-cancel" name = "'+file.idx+'">X</button>'
		+'<img src="'+src+'" /><br>'
		+fileName+
		'</div>'
	return html;
}




/**
 * 위로가기 버튼
 */
$(document).on('click','.scroll-top',function(e) {
	$( 'html, body' ).animate( { scrollTop : 0 }, 400 );
})

/**
 * 아래로 가기
 */
$(document).on('click','.scroll-bottom',function(e) {
	$( 'html, body' ).animate( { scrollTop : $(document).height() }, 400 );
})






