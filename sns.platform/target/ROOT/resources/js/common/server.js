

var serverService = function(url,type,data,callback,errorcallback,formData) {
	var errormsg = '잡시후에 다시 접속해 주세요'
		
	if(!url && !type  && !callback){
		console.log('url '+url+' type '+type+' callback '+callback)
		return;
	}
		
	if(errorcallback==null || !errorcallback) {
		errorcallback = function(DTO) {
			alert('잡시후에 다시 접속해 주세요')
		}
	}	
		
	
	var ajaxobj = {
					url : url,
					type : type,
					data : data,
					success : function(DTO) {
						if (DTO.code > 0 ) {
							callback(DTO.data)
						} else {
							errorcallback(DTO)
						}
					},
					error : function(data) {
						alert('잡시후에 다시 접속해 주세요')
					}
					
				  };
	
	if (formData || formData ==  null) {
		ajaxobj.processData = false;
		ajaxobj.contentType = false;
	};
	
	$.ajax(ajaxobj);
}
	
	
	
	



