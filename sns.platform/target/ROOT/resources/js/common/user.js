/**
 * 유저에 관한 자바 스크립트 (등록 모달 , 수정 모달 , 로그인 모달 , 로그아웃 , 회원탈퇴 )
 */

//현재 사용자
var member = {userNum : $('#login-gnb .container .collapse .nav .dropdown img').attr('name')};

//수정 등록시 사용될 전역객체
var user = { active : false, userFile : null, userType: 'user', checkIdList : new Array(), checkNickList : new Array()};



/**
 * 유저 초기화 함수 
 */
function userinit () {
	user.checkIdList = new Array();
	user.checkNickList = new Array();
	user.userFile = null;
	
}

function userFailInit (){
	user.checkIdList = new Array();
	user.checkNickList = new Array();
	$('.user-modal input[name="userId"], .user-modal input[name="userNick"]').css('color','red');
	$('.user-modal .btn.btn-success.btn-check,.user-modal .user-check-btn').addClass('btn-danger').removeClass('btn-success');
	
}

/**
 * 기존 체크리스트 검토 
 */

function checking (data,list) {
	for (var i = 0, len = list.length; i < len; i++ ){
		if(data == list[i]){
			return true;
		}
	}
	return false;
}


/**
 * 유저 등록 이벤트처리
 */


/**
 * 유저등록 모달 오픈 
 */
$(document).on('click','.singup-open',function(e) {
	var userHtml = htmlMaker.sighupModal();
	userinit();
	$('#modal-singup .modal-body').html(userHtml);
	$('#modal-singup').modal('show')
});

/**
 * 유저등록 모달 닫기 
 */
$(document).on('click','#modal-singup .modal-close,#modal-singup .modal-backdrop',function(e) {
	$('#modal-singup').modal('hide')
});


/**
 * 유저등록 정보 입력  
 */
$(document).on('input','#modal-singup .check-input',function(e) {

	var value = $(this).val();
	var cktype = $(this).attr('name')
	var ck = false;
	var $btnval = {};
	
	if (cktype.match('Id')) {
		ck = checking(value,user.checkIdList)
		$btnval = $('#modal-singup .btn-check[name="checkid"]')
	} else if ((cktype.match('Nick'))) {
		ck = checking(value,user.checkNickList)
		$btnval = $('#modal-singup .btn-check[name="checkname"]')
	}
	
	if (ck) {
		$btnval.addClass('btn-success').removeClass('btn-danger');
		$(this).css('color','black')
	} else {
		$btnval.addClass('btn-danger').removeClass('btn-success');
		$(this).css('color','red')
	}
	
});

/**
 * 비밀번호 재확인
 */
$(document).on('input','#modal-singup input[type="password"]',function(e) {
	var $another = $(this).siblings('input[type="password"]');
	var thisVal = $(this).val();
	var anotherVal = $another.val();
	if ( thisVal == anotherVal ) {
		$('#modal-singup input[type="password"]').addClass('btn-success').removeClass('btn-danger');
	} else {
		$('#modal-singup input[type="password"]').addClass('btn-danger').removeClass('btn-success');
	}
});



/**
 * 유정 정보 중복 체크 
 */

$(document).on('click','#modal-singup .modal-body .btn-check',function(e) {
	
	var cktype = $(this).attr('name');
	
	var cklist = null;
	var $input = null;
	
	if (cktype.match('name')) {
		$input = $('#modal-singup input[name=userNick]')
		cklist = user.checkNickList
	} else if (cktype.match('id')) {
		$input = $('#modal-singup input[name=userId]')
		cklist = user.checkIdList;
	}
	
	var data = $input.val();
	
	if (data == null || data.trim() == '') {
		alert('값을 입력해 주세요')
		$('#modal-singup .btn-check[name="'+cktype+'"').addClass('btn-danger').removeClass('btn-success');
		return;
	} else if (data.length < 3 ){
		alert('3개 이상 입력해주세요')
		$('#modal-singup .btn-check[name="'+cktype+'"').addClass('btn-danger').removeClass('btn-success');
		return;
	}  
	
	if( checking(data,cklist) ){
		alert('이미 확인한 내용입니다')
		return;
	}
	
	var url = '/user/'+cktype+'/'+data;
	console.log(url)
	
	serverService(url,
				 'GET',
				 null,
				 function(){
							alert('사용가능합니다')
							if (cktype.match('id')) {
								user.checkIdList.push(data);
								user.ckId = true;
							
							 } else {
							user.checkNickList.push(data);
							user.ckName = true;
							 }
							$('#modal-singup .btn-check[name="'+cktype+'"').addClass('btn-success').removeClass('btn-danger');
							$input.css('color','black')
						   },
				 function(){
				             alert('이미 사용중입니다')
				  });
});


$(document).on('drop','.profile.dropzone',function(e) {
	var files = e.originalEvent.dataTransfer.files;
	var $modalImg = $('.user-img');
	var imageFile = files[0];
	if (files.length != 1) {
		alert('한개의 이미지만 올려주세요');
		return;
	}
	if (!imageFile.type.match('image')) {
		alert('이미지만 올려주세요');
		return;
	}
	
	var pastURL = $modalImg.attr('src')
	$modalImg.attr('name',pastURL)
	src = window.URL.createObjectURL(imageFile);
	$modalImg.attr('src',src)
	$modalImg.siblings('.img-user-cancel').css('visibility','visible');
	user.userFile = imageFile;
});

/**
 * 프로필 이미지 취소
 */
$(document).on('click','.img-user-cancel',function(e) {
	user.userFile = null;
	var pastURL = $(this).siblings('.user-img').attr('name');
	$(this).siblings('.user-img').attr('src',pastURL)
	$(this).css('visibility','hidden');
});



$(document).on('click','#modal-singup .userRegBtn',function(e) {
	var $danger = $('#modal-singup .btn-danger');
	var $inputs = $('#modal-singup input');
	var warningmsg = '';
	var userData = new FormData();
	
	$inputs.each(function() {
		var value = $(this).val()
		if(!value || value.trim() == '' ){
			warningmsg += ' '+$(this).attr('placeholder');
			return;
		}
		var name = $(this).attr('name')
		if(name){
			userData.append(name,value)
		}
	})
	if (warningmsg != '' ) {
		alert(warningmsg+'을입력해주세요')
		return;
	}
	if($danger.length > 0 ){
		alert('중복을 체크해 주세요');
		return;
	}
	
	if (user.userFile) {
		userData.append('userFile',user.userFile)
	}

	/**
	 * 실패 에대한 로직과 서버 서비스의 실패시 로직 추가 중요!!!
	 */
	
	userData.append('userType','user');
	serverService ('/user',
					'POST',
					userData,
					 function(data){
					                 alert('회원등록 되었습니다');
					                 $('#modal-singup').modal('hide');
					                 $('#modal-login').modal('show');
									},
					 function(data){
					                 var msg = data.message;
									 if(msg.match('DuplicateKeyException')) {
									     alert('죄송합니다 중복을 다시한번 체크해 주세요');
									     userFailInit();
									     //로그인 모달 켜짐
									 } else {
										 userFailInit();
									     alert('죄송합니다 잠시후에 접속해주세요');
									 }
								    },
				   true)	
	});




/**
 * 로그인 이벤트 처리
 */


/**
 * 로그인 모달 오픈 
 */
$(document).on('click','#annony-gnb .login-open',function(e) {
	$('#modal-login').modal('show')
});

/**
 * 로그인 시도
 */
$(document).on('click','#modal-login .btn-login',function(e) {
	var ck = true;
	var userData = new FormData();
	userData.append('userType', 'user');
	$('#modal-login input').each(function(){
		var value = $(this).val();
		var type = $(this).attr('name');
		if (!value || value.trim() == '' ) {
			ck = false;
			alert($(this).attr('placeholder')+' 을 입력해주세요');
			return;
		}
		userData.append(type, value);
	})
	
	if(!ck) {
		return;
	}
	console.log(userData)
	
	serverService('/login',
					'POST',
					userData,
					function(data){
									alert('로그인 되었습니다');
									location.href='/boardView'
	                       		  },
	               function(data){
	                       			alert('패스워드 또는 아이디가 다릅니다');
	                       		 }
		   		 );
});

/**
 * 로그인창 닫을떄
 */
$(document).on('click','#modal-login .modal-close, #modal-login .modal-backdrop',function(e) {
	$('#modal-login').modal('hide');
	$('#modal-login input').val('')
});



/**
 * 회원수정
 */
$(document).on('click','.user-mod-open',function(e) {
	user.active = false;
	
	userinit();
	var html = htmlMaker.userModModal
	$('#modal-user-mod .modal-body').html(html);
	$('#modal-user-mod .userModBtn').addClass('secret');
	$('#modal-user-mod .user-input-pass').attr('disabled','disabled');
	serverService('/user/'+member.userNum,
			'GET',
			null,
			function(data){
							member = data;
							console.log(member);
							$('#modal-user-mod input').each(function() {
								var key = $(this).attr('name')
								if(member[key]) {
									$(this).val(member[key])
								}
							})
							$('#modal-user-mod .user-img').attr('src',member.imageUrl);
							$('#modal-user-mod').modal();
							user.active = true;
						  },
            function(data){
  							alert('탈퇴된 회원입니다')
  							$('#modal-user-mod').modal();
  						  }
                   		  
   	);
	
});



/**
 * 회원수정 비밀번호 체크
 */
$(document).on('click','#modal-user-mod .btn-check-password',function(e) {
	var password = $('#modal-user-mod input[name="userPass"]').val();
	if(password.trim() == '' ){
		alert('비밀번호를 입력해 주세요')
		return;
	}
	var userData = new FormData();
	userData.append('userPass',password)
	serverService('/user/checkpass',
				  'POST',
				   userData,
				   function(data){
					   				$('#modal-user-mod .user-input-pass').val(password);
					   				$('#modal-user-mod input[disabled="disabled"]').not('[name="userId"]').removeAttr('disabled');
					   				$('#modal-user-mod .secret').removeClass('secret');
					   				$('#modal-user-mod .btn-check-password').remove();
					   				$('#modal-user-mod input[name="userPass"]').addClass('user-input-pass');
					   			 },
				   	function(data){
				   				    alert('잘못된 비밀번호 입니다')
				   				    return;
				   				  }
                   		  
   				  );
});


$(document).on('click','#modal-user-mod .btn-check[value="checkname"]',function(e) {
	var userNick = $('#modal-user-mod input[name="userNick"]').val();
	if (!userNick || userNick.trim() == '') {
		alert('별명을 입력해주세요');
		return;
	}
	if (userNick.length < 3) {
		alert('3문자 이상 입력해주세요');
		return;
	}
	if (userNick == member.userNick ) {
		alert('자신의 이름 입니다');
		return;
	}
	for (var i = 0, len = user.checkNickList.length; i < len; i++ ){
		if (user.checkNickList[i] == userNick ) {
			alert('이미 중복확인 한 별명 입니다 ')
			return;
		}
	}
	serverService('/user/checkname/'+userNick,
				  'GET',
				  null,
				  function(data){
							user.checkNickList.push(userNick);
							$('#modal-user-mod btn.btn-danger').addClass('btn-success').removeClass('btn-danger');
							$('#modal-user-mod input[name="userNick"]').css('color','green')
							alert('사용가능 합니다')
						  },
			      function(data){
							$('#modal-user-mod btn.btn-danger').addClass('btn-danger').removeClass('btn-success');
							$('#modal-user-mod input[name="userNick"]').css('color','red')
							alert('중복된 이름이 있습니다')
						  }
                   		  
   	);
});




$(document).on('input','#modal-user-mod .user-input-pass',function(e) {
	if(!user.active) {
		return;
	}
	var $another = $(this).siblings('.user-input-pass');
	var thisVal = $(this).val();
	var anotherVal = $another.val();
	if ( thisVal == anotherVal ) {
		$('#modal-user-mod .user-input-pass').addClass('btn-success').removeClass('btn-danger');
	} else {
		$('#modal-user-mod .user-input-pass').addClass('btn-danger').removeClass('btn-success');
	}
});


$(document).on('input','#modal-user-mod input[name="userNick"]',function(e) {
	if(!user.active) {
		return;
	}
	var userNick = $(this).val();
	if (userNick == member.userNick ) {
		$(this).css('color','black');
		return;
	}
	var ck = false;
	for (var i = 0, len = user.checkNickList.length; i < len; i++ ){
			if (user.checkNickList[i] == userNick ) {
				ck = true;
				break;
			}
	}
	if (ck) {
		$(this).css('color','green');
		$(this).siblings('input.btn-check').addClass('btn-success').removeClass('btn-danger');
	} else {
		$(this).css('color','red');
		$(this).siblings('input.btn-check').addClass('btn-danger').removeClass('btn-success');
	}
});




$(document).on('click','#modal-user-mod .userModBtn',function(e) {
	if( $('#modal-user-mod .btn-danger').length>0 ){
		alert('중복확인해주세요');
		return;
	}
	var userData = new FormData();
	var msg = ''
	$('#modal-user-mod input').each(function(){
		var key = $(this).attr('name');
		var memberVal = member[key];
		var value = $(this).val() 
		if(!value || value.trim() == '' ){
			msg += ' '+$(this).attr('placeholder') ;
			return;
		}
		if(memberVal && memberVal == value){
			return;
		}
		userData.append(key,value)
	})
	
	if(msg != ''){
		alert(msg+'값을 입력해 주세요')
		return;
	}
	
	if (user.userFile) {
		userData.append('userFile',user.userFile);
	}
	
	var userNick = userData.get('userNick')
	serverService ('/user/modify',
					'POST',
					userData,
					function(data){
					                 alert('회원 수정 되었습니다');
					                 $('#modal-user-mod').modal('hide');
					                 var math = Math.random()*100;
					                 $('img[src="/user/'+member.userNum+'/image"]').attr('src','/user/'+member.userNum+'/image?param='+math)
					                 if(userNick) {
					                 $('#login-gnb .navbar-collapse .dropdown label').html(userNick)
					                 $('.div-board-mymsg').parent().find('label').html(userNick)
					                 }
					               },
					function(data){
					                 var msg = data.message;
									 if(msg.match('DuplicateKeyException')) {
									     alert('죄송합니다 중복을 다시한번 체크해 주세요');
									     userFailInit();
									 } else {
										 userFailInit();
									     alert('죄송합니다 잠시후에 접속해주세요');
									 }
								   },
				   true)	
	});
	







