<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
</head>
<body>
	<!-- 회원가입 모달 창 -->
	<div class="modal fade user-modal" id="modal-singup">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close modal-close">×</button>
					<h4 class="modal-title">회원가입</h4>
				</div>
				<div class="modal-body">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-info userRegBtn">가입</button>
					<button type="button" class="btn btn-default modal-close" >닫기</button>
				</div>
			</div>
		</div>
	</div>









	<!-- 로그인 모달 창 -->
	<div class="modal fade" id="modal-login">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close modal-close" >×</button>
					<h4 class="modal-title">로그인</h4>
				</div>
				<div class="modal-body">
						<input class="input-lg " type="text" name="userId"  placeholder="ID"><br>
						<input class="input-lg" type="password" name="userPass"  placeholder="PASSWORD">
						<div class="btn  btn-login">로그인</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-info singup-open modal-close" >회원가입</button>
					<button type="button" class="btn btn-default modal-close" >닫기</button>

				</div>

			</div>
		</div>
	</div>
	
	
	
	
	<!-- 회원수정 모달 창 -->
	<div class="modal fade user-modal" id="modal-user-mod">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close modal-close" >×</button>
					<h4 class="modal-title">회원수정</h4>
				</div>
				<div class="modal-body">
					
					<img class="user-img"  src="/resources/images/eror.jpg" />
					<button class="img-user-cancel" >취소</button>
					<div class="form-group-lg">
						<p>아이디</p>
						<input type="text" class="input-sm" name="userId"  value="삭제된 유저입니다"  placeholder="아이디" disabled = "disabled">
						<p>비밀번호</p>
						<input type="password" class="input-sm"  name="userPass"  placeholder="비밀번호 " >
						<button class='btn btn-danger  btn-check-password' >비밀번호확인</button>
						<p>비밀번호 확인</p>
						<input type="password" class="input-sm user-input-pass"  placeholder="비밀번호 확인" disabled = "disabled">
						<p>별명</p>
						<input class="input-sm " name="userNick" value="삭제된 유저입니다"  placeholder="별명" disabled = "disabled">
						
						<button class='btn btn-success btn-check secret ' value="checkname">중복</button>
						
						<div class="dropzone profile secret">파일을 올려 주세요</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-info userModBtn secret" >수정</button>
					<button type="button" class="btn btn-default modal-close" data-dismiss="modal">닫기</button>
``
				</div>

			</div>
		</div>
	</div>
	
	<!-- 게시글 등록 모달창  -->
	<div class="board-reg-modal modal fade" >
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close modal-board-close" data-dismiss="modal">×</button>
					<h4 class="modal-title">게시글 등록</h4>
				</div>
				<div class="modal-body">
				<p>게시글</p>
				<textarea class="textarea board-textarea"></textarea>
				<div class="dropzone board">파일을 올려 주세요</div>
				<p>파일</p>
				<div class="board-file-list"></div>
				<div class="board-reg-div col-md-12">
					
				</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-info btn-reg">등록</button>
				</div>

			</div>
		</div>
	</div>
	
	<!-- 게시글 수정 모달창  -->
	<div class="modal fade " id="boardModmodal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<p>게시글 수정</p>
					
				</div>
				<div class="modal-body">
					<textarea>
					</textarea>
					<div class="dropzone">파일을 올려 주세요</div>
					<div class="div-files col-md-12">
					
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-info btn-board-mod-com">수정완료</button>
					<button type="button" class="btn btn-default modal-close" data-dismiss="modal">닫기</button>
``			</div>

			</div>
		</div>
	</div>
	
	
	
	
	
	<!-- 게시판모달 -->
	<div class="modal fade modal-board"  title="53">
		<div class="modal-dialog" style="height: 100%;">
			<div class="modal-content col-md-12">
				<div class="modal-header col-md-12">
					<div class="col-md-8">
					<p>죄송합니다 </p>
					</div>
					<div class="col-md-2 modal-header-modify" >
					</div>
					<button type="button" class="close modal-close" >×</button>
				</div>
				<div class="modal-body append " >
					<div class="col-md-12 modal-body-board">
						<div class="col-md-12 modal-body-board-header">
							<div class = "col-md-8 board-reg-user">
								<img class="modal-body-img-board-header profile" src= "/resources/images/eror.jpg">
								삭제된 게시물입니다
							</div>
							<div class = "col-md-4 count">
								<img class="modal-body-img-board-header-count" src= "/resources/images/replyImg.png"> 
								<img class="modal-body-img-board-header-count" src= "/resources/images/replyImg.png">
							</div>
						</div>
						<div class="col-md-12 modal-body-board-body">
							<textarea class="col-md-12 modal-body-textarea" >삭제된 게시물입니다</textarea>
						</div>
					</div>
					<div class="modal-body-reply"> 
						<div class="modal-body-reply-start">
							<img class="modal-body-img-board-header-count" src= "/resources/images/replyImg.png">
						</div>
						<div class="col-md-12 modal-body-reply-header">
							입력창
							<div class="col-md-12 modal-body-reply-one modal-body-reply-one-reg">
								<textarea class="modal-body-reply-one-textarea"></textarea>
								<button class="btn btn-success modal-body-reply-register">입력</button>
							</div>
						</div>
						<div class="col-md-12 modal-body-reply-start">
								댓글 목록
						</div>
						<div class="col-md-12 modal-body-reply-footer">
						
						<div class="col-md-12 modal-body-reply-list">
						
						</div>
						
					</div>
					</div>
				<div class="modal-footer ">
					<button type="button" class="btn btn-default modal-close btn-board-modal-close" >닫기</button>
				</div>
			
		</div>
	</div>
	</div>
	</div>
	
	
	
	
	
	
	<div class="div-file-detail">
		<div class="div-file-header">
			<button class="close file-close">x</button>
		</div>
		<div class="div-file-body">
			
		</div>
		<div class="div-file-footer">
			<a href="#"><button class='btn btn-info btn-xs'>다운</button></a>
		</div>
 
   </div>
	
	
	
	
	
	
	
	
</body>
</html>