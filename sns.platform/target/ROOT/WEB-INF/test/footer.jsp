<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
</head>
<body>
<div id="div-footer" class="col-md-12">
	<div class="col-md-5 col-md-offset-1 footer-left">
		(주) 오픈잇 <br>
		신입 사원 교육 프로젝트  <br>
		SNS 플랫폼  <br>
		작성자 : 사원 박용범 <br>
		작성일 : 2016-11-24 <br>
	</div>
	<div class="col-md-5 col-md-offset-1 footer-right">
		<img src="/resources/images/openit_ci.png" >
	</div>
</div>
<script type="text/javascript" src="/resources/js/common/server.js"></script>
<script type="text/javascript" src="/resources/js/common/html.js"></script>
<script type="text/javascript" src="/resources/js/common/user.js"></script>

</body>
</html>