<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset='utf8'>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>snstemplat</title>
<script src="/resources/jquery/jquery-1.11.3.js"></script>
<script src="/resources/jquery/jquery-ui.js"></script>
<link href="/resources/jquery/jquery-ui.css" rel="stylesheet">
<link href="/resources/css/bootstrap.min.css" rel="stylesheet">
<script src="/resources/js/bootstrap.min.js"></script>
<link href="/resources/css/header.css" rel="stylesheet">
</head>
<body>

<div>
	<c:if test="${user != null }">	
	<nav id="headerNav" class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class='navbar-header'>
			<button class="navbar-toggle navbar-menu" data-toggle="collapse" data-target=".collapse">
				<span>메뉴</span>
			</button>
			<a class="navbar-brand" href="/home">Open it SNS</a>
		</div>
		<div class="collapse navbar-collapse" id='headnav'>
			<ul class="nav navbar-nav navbar-right navbar-custom ">
				<li class="navbar-menu navbar-cus-modal" ><a href="/boardView">게시글</a></li>
				<li class="navbar-menu navbar-cus-modal"><a href="/myBoards">내 게시글</a></li>
				<li class="navbar-menu navbar-cus-modal" ><a   href="#" class="board-reg-btn">글 작성</a></li><!-- 모달 -->
				<li class=" dropdown " value="3">
					<img src="/user/${user.getUserNum()}/image" data-close-others="false" data-delay="0" data-hover="dropdown" 
					data-toggle="dropdown" aria-expanded="false"  name="${user.getUserNum() }"/>${user.userNick }
					<ul class="dropdown-menu">
						<li class="navbar-menu">
							<a class="user-mod-btn">회원수정</a><!-- 모달 -->
						</li>
						<li class="navbar-menu">
							<a class="btn-logout">로그아웃</a>
						</li>
						<li class="navbar-menu">
							<a class="btn-user-del">회원탈퇴</a>
						</li>
					</ul>
				
				</li>
			</ul>
		</div>
	</div>
	</nav>
	</c:if>
	<c:if test="${user == null }">
		<nav id="homeNav" class="navbar navbar-default navbar-fixed-top" data-toggle="collapse" data-target=".collapse">
		<div class="container">
		<div class='navbar-header'>
			<button class="navbar-toggle navbar-menu">
				<span>메뉴</span>
			</button>
			<a class="navbar-brand" href="home">Open it SNS</a>
		</div>
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav navbar-right navbar-custom ">
				<li class="navbar-menu navbar-cus-modal" value="0"><a href="#">로그인</a></li>
				<li class="navbar-menu navbar-cus-modal" value="1"><a href="#">회원가입</a></li>
				
			</ul>
		</div>
	</div>
	</nav>
	</c:if>
</div>
<div class="upper-container">
	
</div>
</body>
</html>