package sns.platform.biz.study;

import java.util.ArrayList;

public class StudyVO {
	String pilsu;
	String id;
	ArrayList<String> list;
	
	@Override
	public String toString() {
		return "StudyVO [pilsu=" + pilsu + ", id=" + id + ", list=" + list + "]";
	}
	public ArrayList<String> getList() {
		return list;
	}
	public void setList(ArrayList<String> list) {
		this.list = list;
	}
	public String getPilsu() {
		return pilsu;
	}
	public void setPilsu(String pilsu) {
		this.pilsu = pilsu;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	

}
