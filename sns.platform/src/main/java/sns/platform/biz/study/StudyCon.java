package sns.platform.biz.study;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class StudyCon {
	
	/*@RequestMapping(value="/released/{seq}/join", method=RequestMethod.POST)
	@ResponseBody
	public void _mobile_pre_release_game_join(@PathVariable(value="seq") Integer gameSeq,
											MobileAuthInterVO auth, ModelMap model, @ModelAttribute GameLogVO param) throws Exception{
		BeanUtils.copyProperties(auth, param);
		
		param.setGameSeq(gameSeq);
		try{
			//game log
			GameLogVO gameLog = new GameLogVO();
			BeanUtils.copyProperties(param, gameLog);
			gameLog.setType("BC");
			gameLog.setGameSeq(gameSeq);
			gameLog.setArea("R");
			
			logMapper.insertGameLog(gameLog);
		} catch (Exception ex){
			logger.error(ex.getMessage(), ex);
		}
		
		joinMapper.SP_MOBILE_GET_COUPON(param);
		logger.info("...join result:" + param.toString());
		if ( SPResultType.RET_CODE_SUCCESS.equals(param.getRetCode())){
			throw new RestfulAPIException(RestfulAPIException.SUCCESS);
		}else{
			throw new RestfulAPIException(HttpStatus.BAD_REQUEST.value(), param.getRetCode(),  param.getRetMsg(), null);
		}
		
	}*/
	
	
	@RequestMapping(value="/test/reqParam", method=RequestMethod.POST)
	public void reqParam( @RequestParam(value="nec", required=true, defaultValue="1")String nec,//String int 만
			@RequestParam MultiValueMap<String, String> params
			){
		
		/**
		 * @RequestParam(value="keys") String [] keys 맵형태일때 키의배열
		 * @RequestParam MultiValueMap<String, String> params
		 */
		Collection<List<String>> obj = params.values();
		for (List<String> list : obj) {
			System.out.println("리퀘스트 파람 맵으로 받기 "+list.toString());
		}
		System.out.println(nec);
		
	}
	@RequestMapping(value="/test/reqmodel", method=RequestMethod.POST)
	public void reqmodel( @ModelAttribute StudyVO nec,StudyVO vo){//get set 기반!!!!!
		System.out.println(nec);
		System.out.println(vo);
	}
	
	@RequestMapping(value="/test/reqbody", method=RequestMethod.POST)
	public void reqbody(@RequestBody StudyVO nec ) {
		
		System.out.println(nec);
		System.out.println(nec.getList().size());
		
		
		
	}
	
	@RequestMapping(value="/test/req", method=RequestMethod.POST)
	public void req(HttpServletRequest req,String ui ) {
	/*Enumeration<String> paramsmap = req.getAttributeNames();
	while (paramsmap.hasMoreElements()) {
		String key = paramsmap.nextElement();
		Object value = req.getAttribute(key);
		System.out.println("키 "+key+" "+"값 "+value.toString()+" 클래스 "+value.getClass().getName());
	}*/
		
		System.out.println(req.getParameter("ui"));
		System.out.println(ui);
	}
	
	@RequestMapping(value="/test/reternmv", method=RequestMethod.GET)
	public @ResponseBody StudyVO reternmv(HttpServletRequest req,String ui ) {
		StudyVO json = new StudyVO();
		json.setId("아이디");
		json.setPilsu("필수");
		ArrayList<String> list =new ArrayList<String>();
		list.add("하나");
		list.add("둘");
		list.add("셋");
		list.add("넷");
		json.setList(list);
		return json;
	}
	


}
