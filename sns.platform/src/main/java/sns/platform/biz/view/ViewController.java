package sns.platform.biz.view;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import sns.platform.common.resolver.AttrMap;
/**
 * <pre>
 * 화면에 대한 컨트롤러
 * </pre>
 * 
 * @author ybpark
 * @version 0.1
 * @since 0.1
 * @created 2016. 11. 15
 * @updated 2016. 11. 15
 */
@Controller
public class ViewController {
	
	/**
	 *  home 페이지
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String homeView(AttrMap map) {
			return "home/home";
	}
	@RequestMapping(value = "/boardView", method = RequestMethod.GET)
	public String boardsView(AttrMap map) {
			return "board/boards";
	}
	
	@RequestMapping(value = "/boardView/{boardNum}", method = RequestMethod.GET)
	public String boardView(AttrMap map, @PathVariable("boardNum")String boardNum,HttpServletRequest req) {
			System.out.println("게시글 번호!!!!!!!!!!!!! "+boardNum);
			int boardNumber = NumberUtils.toInt(boardNum, -1);
			if(boardNumber == -1){
				return "forward:/error/404";
			}
			req.setAttribute("boardNum", boardNumber);
			return "board/board";
	}
	
	@RequestMapping(value = "/myBoards", method = RequestMethod.GET)
	public String myboardsView(AttrMap map) {
			return "board/myboards";
	}
	
	/**
	 * 404에러 페이지
	 * 
	 * @return
	 */
	@RequestMapping(value = "/error/404", method = RequestMethod.GET)
	public String eror404() {
		return "errorPage/404";
	}
	@RequestMapping(value = "/testPage", method = RequestMethod.GET)
	public String testPage() {
		return "test";
	}
	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String testPage(String view) {
		return "test/"+view;
	}


}
