package sns.platform.common.intercepter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;

@Component
@Aspect
public class ParamCheckAOP {

	private static final Logger LOG = LoggerFactory.getLogger(ParamCheckAOP.class);

	/*@Before("execution(public * sns.platform.biz.*.*Controller.*(..))")
	public void connection(JoinPoint jp){
		String fullname = jp.getSignature().toString();
		String msg = "\nmethod :: "+ fullname;
		Object[] args = jp.getArgs();
		LOG.info(msg+"\n"+Arrays.toString(args));
	}*/
	
	/*
	@After("execution(public * sns.platform.biz.*.*Controller.*(..))")
	public void disconnection(ProceedingJoinPoint jp){
		*//**
		 * 후처리
		 *//*
	}*/
	
	@Around("execution(public * sns.platform.biz.*.*Controller.*(..))")
	public Object arround(ProceedingJoinPoint jp) throws Throwable{
		LOG.info("MethodStart...............");
		String fullname = jp.getSignature().toString();
		String msg = "\nmethodname :: "+ fullname;
		Object[] args = jp.getArgs();
		LOG.info(msg+"\n"+Arrays.toString(args));
		
		Object result = jp.proceed();
		
		if(result.getClass().equals(String.class)){
			LOG.info("\nJSP : "+result.toString());
		} else if (result.getClass().equals(ModelAndView.class)){
			ModelAndView mnv = (ModelAndView) result;
			String viewname = mnv.getViewName();
			msg = "\nviewname : "+viewname;
			HashMap<String,Object> map = (HashMap<String, Object>) mnv.getModel();
			Set<String> keys = map.keySet();
			for (String key : keys) {
				msg += "\n"+key+" : "+map.get(key);
			}
			LOG.info(msg);
			
		}
		LOG.info("MethodEnd...............");
		
		return result;
	}
	
	
	
	
	
}
