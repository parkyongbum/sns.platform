/**
 *(주)오픈잇 | http://www.openir.co.kr 
 * Copyright (c)2006-2016 openit Inc.
 * All rights reserved
 */
package sns.platform.common.constant;



import org.springframework.stereotype.Component;

@Component
public class ConstantDefined {
	
	public final static String PROFILE_DIR =  "user";
	public final static String BOARD_DIR = "board";
	public final static String PROFILE_DEFAULT = "default.jpg" ;
	

	
}
