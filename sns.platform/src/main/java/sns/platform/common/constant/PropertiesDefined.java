package sns.platform.common.constant;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PropertiesDefined {

	@Value("#{prop['server.url']}")
	private String address;
	@Value("#{prop['server.FilePath']}")
	private String filepath;
	@Value("#{prop['server.PathSection']}")
	private String section;
	
	public PropertiesDefined() {
		super();
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getFilepath() {
		return filepath;
	}
	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	
		
}
