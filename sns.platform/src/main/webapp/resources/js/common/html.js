
$('body').on('dragenter dragover',function(event){
	event.preventDefault();
});
$('body').on('drop',function(event){
	event.preventDefault();
});





var htmlMaker = {
		
	//유저 등록 모달창 html	
	sighupModal : function () {
				  var html = '<img class="user-img" src="/resources/images/profile_default.jpg" />'+
						   	'<button class="img-user-cancel" >취소</button>'+
						    '<div class="form-group-lg">'+
						    '<p>아이디</p>'+
						    '<input type="text" class="input-sm check-input" name="userId"  placeholder="아이디">'+
						    '<button class="btn btn-danger  btn-check" name="checkid">중복</button>'+
						    '<p>비밀번호</p>'+
						    '<input type="password" class="input-sm" name="userPass"  placeholder="비밀번호 ">'+
						    '<p>비밀번호 확인</p>'+
						    '<input type="password" class="input-sm " name="PASSWORD재확인"  placeholder="비밀번호 재확인">'+
						    '<p>별명</p>'+
						    '<input type="text" class="input-sm check-input" name="userNick"  placeholder="별명">'+
						    '<button class="btn btn-danger  btn-check" name="checkname">중복</button>'+
						    '<div class="dropzone profile">파일을 올려 주세요</div>'+
						    '</div>';
				 return html;
				 },
	userModModal : function () {
					  var html = '<img class="user-img"  src="/resources/images/eror.jpg" />'+
					  			 '<button class="img-user-cancel" >취소</button>'+
					  			 '<div class="form-group-lg">'+
					  			 '<p>아이디</p>'+
					  			 '<input type="text" class="input-sm" name="userId"  value="삭제된 유저입니다"  placeholder="아이디" disabled = "disabled">'+
					  			 '<p>비밀번호</p>'+
					  			 '<input type="password" class="input-sm"  name="userPass"  placeholder="비밀번호 " >'+
					  			 '<button class="btn btn-danger  btn-check-password" >비밀번호확인</button>'+
					  			 '<p>비밀번호 확인</p>'+
					  			 '<input type="password" class="input-sm user-input-pass"  placeholder="비밀번호 확인" disabled = "disabled">'+
					  			 '<p>별명</p>'+
					  			 '<input class="input-sm " name="userNick" value="삭제된 유저입니다"  placeholder="별명" disabled = "disabled">'+
					  			 '<button class="btn btn-success btn-check secret " value="checkname">중복</button>'+
					  			 '<div class="dropzone profile secret">파일을 올려 주세요</div>'+
					  			 '</div>';
					return html;
				 },
	
				 
    boardFiles : function(file,num,imgsrc) {
    				var fileName = file.fileName;
    				var key = 'value';
    				if(!fileName || fileName == null ){
    					fileName = file.name;
    					key='name'
    				}
    	
    	
    				var html = '<div class="col-md-2 file-div" title="'+fileName+'">'+
    						   '<button class="btn btn-xs btn-danger file-cancel" '+key+'="'+num+'">X</button>'+
    						   '<img src="'+imgsrc+'" >'+
    						   '</div>';
		    		return html;
    			},
				 
				 
	boardRegModal : function() {
					var html = '<p>게시글</p>'+
							   '<textarea class="textarea"></textarea>'+
							   '<div class="dropzone board">파일을 올려 주세요</div>'+
							   '<p>파일</p>'+
							   '<div class="board-reg-files col-md-12"></div>';
							
					return html;
				},
				
	board : function(board,change) {
		
		var fileCnt = board.boardVidCnt+board.boardImgCnt+board.boardAudCnt;
	 	var year = board.boardRegDate.substr(0,4);
	 	var month = board.boardRegDate.substr(4,2);
	 	var day = board.boardRegDate.substr(6,2);
	 	var time = board.boardRegDate.substr(8,2);
	 	
	 	var html =  '';
	 	
	 	if (!change) {
	 		html += '<div class="col-md-10 col-md-offset-1 div-board board-open" title="'+board.boardNum+'">';
	 	}
	 	
	 	html += '<div class="div-board-header col-md-12">'+
	 			'<div class="col-md-7">'+
	 			'<img src="user/'+board.userNum+'/image" class="profile"/><label>'+board.userNick+'</label>('+board.userId+')  '+year+' 년  '+month+' 월 '+day+' 일 '+
	 			time+'시'+
	 			'</div>'+
	 			'<div class="col-md-3 div-board-count">'+
	 			'<img src="resources/images/replyImg.png" /> '+board.boardReplyCnt+
	 			'<img src="resources/images/file.png" />' +fileCnt+'</div>';
	 			
	 			if (board.userNum == pageVO.userNum) {
	 				html += '<div class="col-md-2 div-board-mymsg">'+
	 						'<button class="btn btn-xs btn-danger btn-board-del" value = "'+board.boardNum+'">X</button>'+
	 						'<button class="btn btn-xs btn-warning btn-board-modify" value = "'+board.boardNum+'" >U</button>'+
	 						'</div>'
	 			}
	 			
	 	html += '</div>'+
	  			'<div class="col-md-12 div-board-body"><textarea class="board-textareas" >'+
	  			board.boardContent+
	  			'</textarea></div>';
		
	 	if (fileCnt > 0) {
	 		  html += '<div class="col-md-12 div-board-footer">';
	  		  if (board.boardImgCnt > 0) {
	  			html += '<div class="col-md-2">'+
	  			       '<img src="'+board.firstImageUrl+'" /> '+board.boardImgCnt+
	  			       '</div>';
	  		  }
	  		  if (board.boardVidCnt > 0) {
	  			html += '<div class="col-md-2">'+
	  			       '<img src="resources/images/video.png" />  '+board.boardVidCnt+
	  			       '</div>';
	  		  }
	  		  if (board.boardAudCnt > 0) {
	  			html += '<div class="col-md-2">'+
			       '<img src="resources/images/audio.png" />  '+board.boardAudCnt+
			       '</div>';
	  		  }	
	  		 html += '</div>'
	  		}
	 	
	 	if (change) {
	 		return html;
	 	}
	 	
	 	html = '<div class="col-md-10 col-md-offset-1 div-board board-open" title="'+board.boardNum+'">'+
	 			html+
	 		   '</div>';
	 	
	 	return html;
	}	
}