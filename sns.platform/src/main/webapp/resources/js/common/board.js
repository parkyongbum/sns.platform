var board = {files : new Array() , tempfiles : {}};


function typeSrcMaker (file){
	var server = true;
	var type = file.fileType;
	if(!type || type == null ){
		type = file.type;
		server = false;
	}
	
	var src = '';
	
	if (type.match('image')) {
		if (server) {
			src = file.fileUrl
		} else {
			blobURL = window.URL.createObjectURL(file);
			src = blobURL;
		}
	} else if (type.match('video')) {
		src = '/resources/images/video.png';
	} else if (type.match('audio')) {
		src = '/resources/images/audio.png';
	} else {
		src = null;
	}
	return src;
}  




$(document).on('click','#login-gnb .nav .navbar-menu .board-reg-btn',function(event){
	board = {files : new Array() , tempfiles : {}};
	var html = htmlMaker.boardRegModal();
	$('#modal-board-reg .modal-body').html(html);	
	$('#modal-board-reg').modal()
});


$(document).on('drop','#modal-board-reg .dropzone.board',function(event){
	var html = '';
	var files = event.originalEvent.dataTransfer.files;
	for(var i = 0, len=files.length; i<len; i++ ) {
		var src = typeSrcMaker(files[i]);
		if( src != null || src ) {
			html += htmlMaker.boardFiles(files[i],board.files.length,src);
			board.files.push(files[i]);
		}
	}
	$('#modal-board-reg .board-reg-files').append(html);
	
});

$(document).on('click','.file-cancel',function(event){
	var idx = $(this).attr('name');
	var fileNum = $(this).attr('value');
	if ( idx ) {
		board.files[idx] = null;
		$(this).parent().fadeOut(1000,function(){
			$(this).remove();
		});	
	} else if (fileNum) {
		var src = $(this).siblings('img').attr('src');
		var name = $(this).parent().attr('title');
		var fileVO = { src : src , filename : name}
		tempfiles[fileNum] = fileVO
		$(this).parent().attr('title','클릭하시면 삭제가 취소 됩니다');
		$(this).siblings('img').attr('src','resources/images/eror.jpg');
		$(this).siblings('img').attr('name',fileNum);
		$(this).siblings('img').addClass('del-files');
		$(this).hide();
	}
});


$(document).on('click','#modal-board-reg .btn-reg',function(event){
	
	var boardDate = new FormData();
	var boardContent = $('#modal-board-reg textarea').val();
	if(!boardContent || boardContent.trim() == '' ) {
		alert('게시글을 입력해 주세요')
		return;
	}
	boardDate.append('boardContent',boardContent)
	
	for (var i = 0,len = board.files.length; i<len; i++ ){
		if(board.files[i]){
			boardDate.append('files',board.files[i])
		}
	}
	
	serverService ('/board',
			'POST',
			boardDate,
			 function(data){
			                 alert('게시글이 등록 되었습니다');
			                 $('#modal-board-reg').modal('hide');
			                /**
			                 * 리페이징
			                 */
							},
			 null,
			 true);	
});	
	
	






