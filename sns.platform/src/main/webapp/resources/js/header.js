/**
 * 헤더의 객체
 */
var $headerNav = $('#headerNav');
var $headerNavCon = $('#headerNav .container');
var $body = $('body');
$body.on('dragenter dragover', function(event) {
	event.preventDefault();
});
$body.on('drop', function(event) {
	event.preventDefault();
});

/**
*사용자 관련
 */
var $userModBtn = $headerNavCon.find('.navbar-collapse .dropdown ul li .user-mod-btn');
var $userModModal = $('.user-mod-modal');
var $userIpts = $('.user-mod-modal .user-input');
var $userPassCkBtn = $userModModal.find('.modal-dialog .modal-content .modal-body .user-pass-btn');
var $userPassIpt = $('.user-mod-modal .user-input[name=userPass]');
var $userCkBtn = $userModModal.find('.modal-dialog .modal-content .modal-body .user-check-btn');
var $userDropZone = $userModModal.find('.modal-dialog .modal-content .modal-body .dropzone');
var $userModalModBtn = $userModModal.find('.modal-dialog .modal-content .modal-footer .modal-user-mod-btn');
var $userModalCloseBtn = $userModModal.find('.modal-close');
var userDTO = {usernickCk : false, userPassCk: false, nickNameck:false};





/**
 * 로그아웃 클릭시
 */
$(document).on('click','.navbar-collapse .dropdown ul li .btn-logout',function(e) {
	$.ajax({
		url : '/logout',
		type : 'GET',
		success : function(data) {
			console.log(data)
			if (data.code > 0) {
				alert('로그아웃 되었습니다');
				location.replace('/home')
			} else {
				alert('잠시후 다시 접속해 주세요');
			}
		},
		error : function (data) {
			alert('잠시후 다시 접속해 주세요');
		}
	});
});

/**
 * 회원탈퇴
 */
$(document).on('click','.btn-user-del',function(e) {
	var userNum = $('#headerNav .container .collapse .nav .dropdown img').attr('name');
	var returnValue = confirm("정말 탈퇴 하시겠습니까");
	if(returnValue) {
	$.ajax({
		url : '/user',
		type : 'DELETE',
		success : function(data) {
			if (data.code > 0) {
				alert('회원탈퇴 되었습니다');
				location.replace('/home')
			} else {
				alert('잠시후 다시 접속해 주세요');
			}
		},
		error : function (data) {
			alert('잠시후 다시 접속해 주세요');
		}
	});
	} else {
		return;
	}
});



/**
 * 회원수정시 이름입력시 이벤트
 */
$(document).on('input','.user-mod-modal input[name="checkname"]',function(e) {
	if(userDTO.nickNameck) {
	$(this).css('color', 'red');
	userDTO.usernickCk = false;
	$userCkBtn.removeClass('btn-success').addClass('btn-danger');
	}
});


/**
 * 회원수정시 버튼 클리시 모달 활성화
 */
$userModBtn.on('click', function(e) {
	$userPassCkBtn.show();
	$userCkBtn.hide();
	$userDropZone.hide();
	$userModModal.modal();
	$userModModal.find('.modal-footer .modal-user-mod-btn').css('visibility','hidden');
	$userModModal.find('.modal-body .modal-img-cancel').css('visibility','hidden');
	
});

/**
 * 회원수정시 현재 비밀번호 체크 
 */
$userPassCkBtn.on('click', function(e) {
	var userPass = $userPassIpt.val();
	$.ajax({
		url : '/user/checkpass',
		type : 'POST',
		data : {
			'userPass' : userPass
		},
		success : function(data) {
			console.log(data)
			if (data.code == 0) {
				alert('비밀번호가 다릅니다');
			} else {
				for (var i = 0, len = $userIpts.length; i < len; i++) {
					$($userIpts[i]).removeAttr('disabled')
				}
				userDTO.usernickCk = true;
				userDTO.userPassCk = true;
				$userPassCkBtn.siblings('input[name=userPassCheck]').val(userPass)
				$userCkBtn.show();
				$userDropZone.show();
				$userPassCkBtn.hide();
				$userModModal.find('.modal-footer .modal-user-mod-btn').css('visibility','visible');
			}
		}
	});
});

/**
 * 중복 체크 버튼 클릭시
 */
$userCkBtn.on('click', function(e) {
	var selector = '.user-mod-modal .user-input[name=' + $(this).val() + ']';
	var thisselector = '.user-mod-modal button[value=' + $(this).val() + ']';
	var $thisIpt = $(selector);
	var res = $thisIpt.val();
	var Url = '/user/' + $(this).val() + '/' + res;
	userDTO.nickNameck = true;
	$.ajax({
		url : Url,
		type : 'GET',
		success : function(data) {
			console.log(data)
			if (data.code == 0) {
				alert('불가능한 이름입니다');
				$thisIpt.css('color', 'red');
				userDTO.usernickCk = false;
				$userCkBtn.removeClass('btn-success').addClass('btn-danger')
			} else if (data.message.indexOf('본인의 이름입니다') > -1) {
				alert('본인의 이름입니다');
				$thisIpt.css('color', 'black');
				userDTO.usernickCk = true;
				userDTO.userNick = null;
				$(thisselector).removeClass('btn-danger').addClass('btn-success')
			} else {
				alert('사용가능한 이름 입니다');
				$thisIpt.css('color', 'black');
				$(thisselector).removeClass('btn-danger').addClass('btn-success')
				userDTO.usernickCk = true;
				userDTO.userNick = res;
			}
		}
	});

})

/**
 * 비밀번호 변경 요구시 인풋할떄
 */
$userIpts.siblings('[name=userPassCheck],[name=userPass]').on('input',function(e){
	var $userPassIpts = $userIpts.siblings('[name=userPassCheck],[name=userPass]')
	if($(this).siblings('[name=userPassCheck]').attr('disabled')){
		return;
	}
	if($userPassIpts.eq(0).val() == $userPassIpts.eq(1).val()) {
		$userPassIpts.css('background-color','green')
		userDTO.userPass = $userPassIpts.eq(0).val();
		userDTO.userPassCk = true;
		return;
	}
	userDTO.userPassCk = false;
	userDTO.userPass = null;
	$userPassIpts.css('background-color','red')
	
})



/**
 * 회원수정시 파일존 드랍
 */
$userDropZone.on('dragenter dragover',function(event){
	event.preventDefault();
})

/**
 * 회원수정시 프로필이미지 드랍시 
 */
$userDropZone.on('drop',function(event){
	event.preventDefault();
	var files = event.originalEvent.dataTransfer.files;
	var $modalImg = $userModModal.find('.modal-body  .modal-img');
	var imageFile = files[0];
	if (files.length != 1) {
		alert('한개의 이미지만 올려주세요');
		return;
	}
	if (!imageFile.type.match('image')) {
		alert('이미지만 올려주세요');
		return;
	}
	var picReader = new FileReader();
	picReader.addEventListener("load", function(event) {
		var image = event.target;
		$modalImg.attr('src', image.result);
		$modalImg.attr('title', image.name);

	});
	picReader.readAsDataURL(imageFile);
	$modalImg.siblings('.modal-img-cancel').css('visibility','visible');
	userDTO.userFile = imageFile;
	
})

/**
 * 프로필수정 파일 취소
 */
$userModModal.find('.modal-body .modal-img-cancel').on('click',function(event){
	var userNum = $('#userId').attr('name');
	var $modalImg = $userModModal.find('.modal-body  .modal-img');
	$(this).css('visibility','hidden');
	$modalImg.attr('src', '/user/'+userNum+'/image');
	userDTO.userFile = null;
});

/**
 * 회원 수정완료 버튼 클릭시
 */
$userModalModBtn.on('click',function(event){
	console.log("이름중복 "+userDTO.usernickCk);
	if(!userDTO.usernickCk){
		alert('이름중복을 체크해주세요')
		return;
	}
	if(!userDTO.userPassCk){
		alert('비밀번호 중복을 체크해주세요')
		return;
	}
	var userNum = $('#headerNav .container .collapse .nav .dropdown img').attr('name');
	var userFormData = new FormData();
	if(userDTO.userPass){
	userFormData.append('userPass', userDTO.userPass);
	}
	if(userDTO.userNick) {
	userFormData.append('userNick', userDTO.userNick);
	}
	if(userDTO.userFile) {
	userFormData.append('userFile', userDTO.userFile);
	}
	$.ajax({
		url : '/user/modify',
		data : userFormData,
		processData : false,
		contentType : false,
		type : 'POST',
		success : function(data) {
			console.log(data.code);
			console.log(data.message);
			var code = data.code;
			var message = data.message;
			var data = data.data;
			if (code == 0) {
				if (message.indexOf('DuplicateKeyException') > -1) {
					alert('중복된 이름또는 아이디가 있습니다');
				} else {
					alert('죄송합니다 잠시후 다시 접속해주세요')
				}
			} else {
				alert('회원수정 처리 되었습니다');
				$userModModal.modal('hide');
				userModalRepage();
				var math = Math.random()*100;
				$('img[src="user/'+userNum+'/image"]').attr('src','user/'+userNum+'/image?param='+math)
			}
		},
		error : function(data) {
			alert('중복된 이름 또는 아이디가 있습니다')
			userFormData.reset;
		}
	})
})

/**
 * 회원수정 모달을 닫을경우
 */
$userModalCloseBtn.on('click',function(e){
	userModalRepage();
})

/**
 * 회원수정 모달 초기화
 * @returns
 */
function userModalRepage(){
	var $modalImg = $userModModal.find('.modal-body  .modal-img');
	var userNum = $('#userId').attr('name');
	$.ajax({
		url : '/user/'+userNum,
		type : 'GET',
		success : function(data) {
			if(data.code>0){
				var userNick = data.data.userNick;
				var math = Math.random()*100;
				var imageUrl = data.data.imageUrl+'?fakeparam='+math;
				$userIpts.siblings('[name=userPassCheck],[name=userPass]').val("");
				$userIpts.siblings('[name=checkname]').val(userNick);
				$userIpts.siblings('[name=userPassCheck],[name=checkname]').attr('disabled','disabled');
				$headerNavCon.find('.collapse li.dropdown img').attr('src',imageUrl);
				$modalImg.attr('src',imageUrl);
			}
		},
		error : function(data) {
			alert('중복된 이름 또는 아이디가 있습니다')
			userFormData.reset;
		}
	})
	
}

//게시판 관련
var $boardRegBtn = $headerNavCon.find('.navbar-collapse .navbar-menu .board-reg-btn');
var $boardRegModal = $('.board-reg-modal');
var $boardRegModalBody = $boardRegModal.find('.modal-dialog .modal-content .modal-body');
var $boardRegModalFileDiv = $boardRegModal.find('.modal-dialog .modal-content .modal-body .board-file-list');
var $boardRegModalFileZone = $boardRegModal.find('.modal-dialog .modal-content .modal-body .dropzone.board');
var $boardRegModalBtn = $boardRegModal.find('.modal-dialog .modal-content .modal-footer .btn-reg');

var boardVO = {files:new Array()}

/**
 * 게시판 등록 클리시
 */
$boardRegBtn.on('click',function(){
	$boardRegModal.modal();
	
});

/**
 * 게시글 등록 모달 드랍존 이벤트
 */
$boardRegModalFileZone.on('dragenter dragover',function(event){
	event.preventDefault();
});

/**
 * 게시글 등록시 모달의 드랍존에 파일 드랍시
 */
$boardRegModalFileZone.on('drop',function(event){
	event.preventDefault();
	var files = event.originalEvent.dataTransfer.files;
	for(var i = 0, len=files.length; i<len; i++ ) {
		var html = fileHtml(files[i],boardVO)
		if (html) {
			$boardRegModalFileDiv.append(html);
		}
	}
});

/**
 * 게시글 등록 모달 닫을경우
 */
$boardRegModal.find(' .modal-board-close').on('click',function(e){
	boardRepage();
})

/**
 * 게시글 등록 파일 취소시
 */
$boardRegModalFileDiv.on('click','.file-cancel', function(event) {  
	var fileidx = $(this).val();
	$(event.target).parent().remove();
	var files = boardVO.files;
	files[fileidx] = null;
});

/**
 * 게시글 등록완료 버튼 클릭시 실제 등록
 */
$boardRegModalBtn.on('click',function(event){
	var boardContent = $boardRegModalBody.find('textarea').val();
	if (!boardContent) {
		alert("내용을 입력해 주세요");
		return;
	}
	var formData = new FormData();
	formData.append('boardContent',boardContent);
	var boardFiles = boardVO.files;
	for (var i = 0, len = boardFiles.length; i < len; i++) {
		if(boardFiles[i]){
			formData.append("files", boardFiles[i]);
		}
	}
	$.ajax({
		url : '/board',
		data : formData,
		processData : false,
		contentType : false,
		type : 'POST',
		success : function(data) {
			console.log(data.code);
			console.log(data.message);
			var code = data.code;
			var message = data.message;
			var data = data.data;
			if (code == 0) {
					alert('죄송합니다 잠시후 다시 접속해주세요')
			} else {
				alert('게시물이 등록 되었습니다');
				location.href='/myBoards';
				
			}
		},
		error : function(data) {
			alert('죄송합니다 잠시후 다시 접속해주세요')
		}
	})
	
})

/**
 * 게시글 등록 모달 초기화
 * @returns
 */
function boardRepage (){
	boardVO = {files:new Array()}
	$boardRegModalBody.find('textarea').val(' ');
	$boardRegModalFileDiv.html(' ')
}

/**
 *  파일 드랍시 파이에대한 html만들고 바이너리저장
 *  
 * @param file 등록 파일의 정보
 * @param boardVO 등록 예정 게시글 정보
 * @returns file에 대한 html
 */
function fileHtml (file,boardVO){
	var files = boardVO.files;
	var fileName = file.name;
	var src = '';
	file.idx = files.length;
	if (file.type.match('image')) {
		files.push(file);
		blobURL = window.URL.createObjectURL(file);
		src = blobURL;
	} else if (file.type.match('video')) {
		files.push(file);
		src = '/resources/images/video.png';
		
	} else if (file.type.match('audio')) {
		files.push(file);
		src = '/resources/images/audio.png';
	} else {
		return null;
	}
	var html = 
		'<div class="col-md-2 file-div" title="'+fileName+'">'
		+'<button class="btn btn-xs btn-danger file-cancel" value = "'+file.idx+'">X</button>'
		+'<img src="'+src+'" /><br>'
		+fileName+
		'</div>'
	return html;
}


