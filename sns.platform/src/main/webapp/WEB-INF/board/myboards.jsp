<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta charset='utf8'>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>snstemplat</title>
</head>
<%@include file="/WEB-INF/include/header.jsp"%>
<body>
<button class="btn btn-info scroll-top">위로가기</button>
<button class="btn btn-info scroll-bottom">아래로가기</button>
	<!-- 메인 컨테이너  -->
	<div class="container main-container">
		<!-- 검색을 위한 영역  -->
		<div class="col-md-12 div-upper">
			<!-- 검색인풋  -->
			<input class="input-group-sm ipt-search" type='text' placeholder="YYYY-MM-DD" name="mb">	
			<!-- 검색버튼 -->
			<button class = "btn btn-info btn-sm btn-searchboard">검색</button>
		</div>
		<!-- 게시판 목록을 보여줄 영역 -->
		<div class="col-md-12 div-boards">
			<!-- 게시판 목록  -->
		</div>
	</div>

	<!--모달 -->
	<%@include file="/WEB-INF/include/modal.jsp"%>
	<%@include file="/WEB-INF/include/footer.jsp"%>
	<script type="text/javascript" src="/resources/js/boardModal.js"></script>
	<script type="text/javascript" src="/resources/js/boardCommon.js"></script>
	<script type="text/javascript" src="/resources/js/boardmoddel.js"></script>
	</body>
</html>