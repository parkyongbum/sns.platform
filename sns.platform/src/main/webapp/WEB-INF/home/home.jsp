<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta charset='utf8'>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>snstemplat</title>
</head>
<%@include file="/WEB-INF/test/header.jsp"%>

<body>

<!-- 부트스트랩 카로우젤 -->
	<div id="homeCarousel" class="carousel slide" data-ride="carousel" >
		<!-- 카로우젤의 모든 이미지 목차 및 찾기 -->
		<ol class="carousel-indicators">
    		<li data-target="#homeCarousel" data-slide-to="0" class="active"></li>
    		<li data-target="#homeCarousel" data-slide-to="1"></li>
    		<li data-target="#homeCarousel" data-slide-to="2"></li>
    		<li data-target="#homeCarousel" data-slide-to="3"></li>
  		</ol>
		<!-- 실제 div 및 이미지  -->
		<div class="carousel-inner" role="listbox">
    		<div class="item active">
      			<img src="/resources/images/home_img_1.png" alt="죄송합니다 이미지를 찾을수 없습니다">
      			<div class="carousel-caption">
        			<h3>오픈잇 사원 교육 sns 플랫폼</h3>
        			<p>작성자 : 박용범</p>
      			</div>
    		</div>

    		<div class="item">
      			<img src="/resources/images/home_img_2.png" alt="죄송합니다 이미지를 찾을수 없습니다">
    		</div>

    		<div class="item">
      			<img src="/resources/images/home_img_3.png" alt="죄송합니다 이미지를 찾을수 없습니다">
    		</div>

    		<div class="item">
      			<img src="/resources/images/home_img_4.png" alt="죄송합니다 이미지를 찾을수 없습니다">
    		</div>
  		</div>
		
		<a class="left carousel-control" href="#homeCarousel" role="button" data-slide="prev">
    		<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    		<span class="sr-only">Previous</span>
  		</a>
  		<a class="right carousel-control" href="#homeCarousel" role="button" data-slide="next">
    		<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    		<span class="sr-only">Next</span>
	   </a>
	</div>




<%@include file="/WEB-INF/test/modals.jsp"%>
<%@include file="/WEB-INF/test/footer.jsp"%>
	
	
	
	<script>
	</script>
	
</body>
</html>