package test;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


import javax.inject.Inject;
import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;


import sns.platform.biz.user.UserDAO;
import sns.platform.biz.user.UserService;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/context/rootContext.xml","file:src/main/webapp/WEB-INF/context/servletconfig/servletContext.xml"})
@WebAppConfiguration
public class MariaDBConnectionTest {

	
	
	@Inject
	DataSource ds;
	
	@Inject
	SqlSessionFactory sessionFactory;
	
	@Inject
	SqlSession session;
	
	
	@Inject
	UserDAO dao;
	
	@Inject
	UserService service;
	
	
	@Test
	public void testConnection() throws ClassNotFoundException {
		getClass().forName("org.mariadb.jdbc.Driver");
		Connection con = null;
		try {
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/test","root","openit");
		} catch (SQLException e) {
			System.out.println("Connection..............fail");
			e.printStackTrace();
			return;
		}
		System.out.println("Connection..............succcess");
		System.out.println(con);
		
	}
	
	@Test
	public void testDataSource() throws ClassNotFoundException {
		Connection con = null;
		try {
			con = ds.getConnection();
		} catch (SQLException e) {
			System.out.println("DataSource Connection..............fail");
			e.printStackTrace();
			return;
		}
		System.out.println("DataSource Connection..............succcess");
		System.out.println(con);
		
	}
	
	@Test
	public void testSqlSessionFactory()  {
		System.out.println(session.selectOne("TestMapper.time").toString());
		
	}
	

	
	
	
}
