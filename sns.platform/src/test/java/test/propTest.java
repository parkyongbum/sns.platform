package test;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import sns.platform.common.constant.ConstantDefined;
import sns.platform.common.constant.PropertiesDefined;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/context/rootContext.xml","classpath:/context/rootContext.xml"})
@WebAppConfiguration
public class propTest {
	
	@Value("#{prop['server.url']}")
	public  String test;
	
	@Inject
	private PropertiesDefined prop;

	@Test
	public void Daoinsert() {
		System.out.println(test);
	}
}
