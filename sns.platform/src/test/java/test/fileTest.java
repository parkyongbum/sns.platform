package test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import sns.platform.biz.board.BoardDAO;
import sns.platform.biz.file.FileDAO;
import sns.platform.biz.reply.ReplyService;
import sns.platform.common.resolver.AttrMap;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/context/rootContext.xml",
		"file:src/main/webapp/WEB-INF/context/servletconfig/servletContext.xml" })
@WebAppConfiguration
public class fileTest {

	@Inject
	BoardDAO dao;
	
	@Inject
	FileDAO fdao;


	
	@Test
	public void Daoinsert() {
		Map<String,Object> map = new HashMap<>();
		map.put("userNum", 32);
		map.put("boardContent", "hihello");
		int boardNum = dao.insert(map); 
		map.put("boardNum", boardNum);
		map.put("boardImgCnt",8);
		map.put("boardAudCnt",8);
		map.put("boardVidCnt",8);
		
		map.put("fileName", "test.jpg");
		map.put("filePath", "\\board\\20161110\\" + UUID.randomUUID().toString() + "_upt.jpg");
		map.put("fileType", "image/jpeg");
		map.put("fileSize", 15.123);
		fdao.insert(map);
		fdao.insert(map);
		dao.update(map);
		
	}
	
	@Test
	public void dellist() {
		List<Integer> list  = new ArrayList<>();
		Map<String,Object> map = new HashMap<>();
		
		for (int i = 0; i < 10; i++) {
			list.add(i);
		}
		map.put("delFiles", list);
		System.out.println(fdao.deleteFiles(map));
		
		
	}

	
	
	
}
