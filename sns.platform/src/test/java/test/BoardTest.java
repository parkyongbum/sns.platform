package test;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import sns.platform.biz.file.FileDAO;
import sns.platform.biz.reply.ReplyService;
import sns.platform.common.resolver.AttrMap;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/context/rootContext.xml",
		"file:src/main/webapp/WEB-INF/context/servletconfig/servletContext.xml" })
@WebAppConfiguration
public class BoardTest {

	@Inject
	FileDAO dao;

	@Inject
	ReplyService service;

	@Test
	public void Daoinsert() {
		AttrMap map = new AttrMap();
		for (int i = 0; i < 5; i++) {
			map.put("boardNum", 33);
			map.put("fileName", "test.jpg");
			map.put("filePath", "\\board\\20161110\\" + UUID.randomUUID().toString() + "_test.jpg");
			map.put("fileType", "image/jpeg");
			if(i%2 == 0){
				map.put("fileType", "audio/mp3");
			}
			map.put("fileSize", 15.123);
			dao.insert(map.getMap());
		}
	}
	
	@Test
	public void searchTest() {
		
	}

	@Test
	public void Serviceinsert() {
		AttrMap map = new AttrMap();
		map.put("boardNum", 33);
		map.put("userNum", 32);
		map.put("replyContent", "servicetesting");
		int result = service.createReply(map);
		System.out.println(result);
	}

	@Test
	public void update() {
		AttrMap map = new AttrMap();
		map.put("boardNum", 456);
		map.put("replyNum", 90);
		map.put("replyContent", "change");
		int result = service.modifyReply(map);
		System.out.println(result);
	}

	@Test
	public void delete() {
		AttrMap map = new AttrMap();
		map.put("boardNum", 33);
		map.put("replyNum", 90);
		int result = service.removeReply(map);
		System.out.println(result);
	}

	@Test
	public void LIST() {
		AttrMap map = new AttrMap();
		map.put("boardNum", 33);
		map.put("lastReplyNum", 136);
		map.put("pageNum", 5);
		List<Map<String, Object>> list = service.searchReplys(map);
		for (Map<String, Object> reply : list) {
			System.out.println(reply.get("replyNum"));
		}
	}

	@Test
	public void deletelist() {
		AttrMap map = new AttrMap();
		map.put("boardNum", 33);
	
	}

}
